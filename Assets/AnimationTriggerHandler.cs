﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class AnimationTriggerHandler : MonoBehaviour
{
    public MMFeedbacks _smashFeedback;


    public void PlaySmashFeedbacks()
    {
        _smashFeedback?.PlayFeedbacks();
    }
}
