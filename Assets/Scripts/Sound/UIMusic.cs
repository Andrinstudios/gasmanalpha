using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class UIMusic : MonoBehaviour
{
    public AudioClip music;

    // Start is called before the first frame update
    void Start()
    {
       // MMSoundManager.Instance.StopTrack(MMSoundManager.MMSoundManagerTracks.UI);
        MMSoundManagerPlayOptions options = MMSoundManagerPlayOptions.Default;
        options.Loop = true;
        options.Location = Vector3.zero;
        options.MmSoundManagerTrack = MMSoundManager.MMSoundManagerTracks.UI;

        MMSoundManagerSoundPlayEvent.Trigger(music, options);
    }

}
