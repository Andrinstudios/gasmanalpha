using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMLevelMusicHandler : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public enum MusicType
    {
        Background = 1,
        Boss,
        MiniBoss,
        UI
    }

    public LevelMusicSO _musicSO;

    private bool isBossMusicPlaying = false;
    private bool isMiniBossMusicPlaying = false;

    private AudioSource _bossAudioSource;
    private AudioSource _miniBossAudioSource;
    private AudioSource _bgMusicAudioSource;
//    private MMSoundManager _SoundManager;

   
    private MMSoundManagerSound backGroundMusicSound;
    private MMSoundManagerSound bossMusicSound;
    private MMSoundManagerSound miniBossMusicSound;

    private void Awake()
    {
        //_SoundManager = MMSoundManager.Instance;

        //if (_SoundManager != null)
        //{
        //    _bossAudioSource = _SoundManager.FindByID((int)MusicType.Boss);
        //    _miniBossAudioSource = _SoundManager.FindByID((int)MusicType.MiniBoss);
        //    _bgMusicAudioSource = _SoundManager.FindByID((int)MusicType.Background);

        //}

        ConvertToSoundManagerSounds();
    }

    public void OnMMEvent(CorgiEngineEvent _event)
    {
        switch (_event.EventType)
        {
            case CorgiEngineEventTypes.Pause:
                SwitchToUIMusic();
                break;
            case CorgiEngineEventTypes.UnPause:
                ReturnToGameMusicFromUI();
                break;
            case CorgiEngineEventTypes.PlayerDeath:
                PlayerDeathMusicHandler();
                break;
            case CorgiEngineEventTypes.Respawn:
                RespawnMusicHandler();
                break;
            case CorgiEngineEventTypes.LevelStart:
                LevelStartMusicHandler();
                break;
            default:
                break;
        }
    }

    protected void SwitchToUIMusic()
    {
        //MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.Music);
        //MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.Sfx);
        //MMSoundManager.Instance.PlayTrack(MMSoundManager.MMSoundManagerTracks.UI);

        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PauseTrack, MMSoundManager.MMSoundManagerTracks.Music);
        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PauseTrack, MMSoundManager.MMSoundManagerTracks.Sfx);
        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PlayTrack, MMSoundManager.MMSoundManagerTracks.UI);

    }

    protected void ReturnToGameMusicFromUI()
    {
        //MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.UI);
        //MMSoundManager.Instance.PlayTrack(MMSoundManager.MMSoundManagerTracks.Music);
        //MMSoundManager.Instance.StopTrack(MMSoundManager.MMSoundManagerTracks.Sfx);

        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PauseTrack, MMSoundManager.MMSoundManagerTracks.UI);
        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PlayTrack, MMSoundManager.MMSoundManagerTracks.Sfx);
        MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PlayTrack, MMSoundManager.MMSoundManagerTracks.Music);

    }

    protected void ReturnToBackgrounMusic()
    {
        if (isBossMusicPlaying)
        {
            isBossMusicPlaying = false;
            //fade out boss and bring in BG
            MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Boss, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
            MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Background, 0.2f, 1, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
        }

        if (isMiniBossMusicPlaying)
        {
            isMiniBossMusicPlaying = false;
            MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.MiniBoss, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
            MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Background, 0.2f, 1, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
        }
    }

    protected void PlayerDeathMusicHandler()
    {
        Debug.Log("deathMusic");
        // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Background, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
        // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Death, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));

    }

    protected void RespawnMusicHandler()
    {
        Debug.Log("Respawn Music");


        // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Respawn, 0.1f, 10.0f, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));

    }

    protected void LevelStartMusicHandler()
    {
        Debug.Log("level started");
        MMSoundManager.Instance.PlayTrack(MMSoundManager.MMSoundManagerTracks.Music);
    }

    protected void StartBossMusic()
    {
        //fade out background music and bring in boss music
        isBossMusicPlaying = true;
    }

    protected void StartMiniBossMusic()
    {
        isMiniBossMusicPlaying = true;
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    private void ConvertToSoundManagerSounds()
    {
        _bgMusicAudioSource = gameObject.AddComponent<AudioSource>();
        _bgMusicAudioSource.clip = _musicSO.backgroundMusic;
        _bgMusicAudioSource.loop = true;

        backGroundMusicSound.Source = _bgMusicAudioSource;
        backGroundMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        backGroundMusicSound.ID = 1;
        backGroundMusicSound.Persistent = false;

        _bossAudioSource = gameObject.AddComponent<AudioSource>();
        _bossAudioSource.clip = _musicSO.bossMusic;
        _bossAudioSource.loop = true;

        bossMusicSound.Source = _bossAudioSource;
        bossMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        bossMusicSound.ID = 2;
        bossMusicSound.Persistent = false;

        _miniBossAudioSource = gameObject.AddComponent<AudioSource>();
        _miniBossAudioSource.clip = _musicSO.miniBossMusic;
        _miniBossAudioSource.loop = true;

        miniBossMusicSound.Source = _miniBossAudioSource;
        miniBossMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        miniBossMusicSound.ID = 3;
        miniBossMusicSound.Persistent = false;

        Debug.Log("sound " + MMSoundManager.Instance.FindByID(1).clip.name);
    }
}
