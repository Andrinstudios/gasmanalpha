using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class GMMusicHandler : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public enum MusicType
    {
        Background = 1,
        Boss,
        Death,
        Respawn,
        UI
    }

    public AudioSource bgSource;
    private MMSoundManagerSound bgMMSMS;

    public MMFeedbacks backgroundMusicFeedback;
    public MMFeedbacks resumeBackgroundMusicFeedback;
    public MMFeedbacks bossMusicFeedback;
    public MMFeedbacks UIMusicFeedback;
    public MMFeedbacks PlayerDeathMusicFeedback;
    public MMFeedbacks PlayerRepawnMusicFeedback;

    public LevelMusicSO _musicSO;

    private void Awake()
    {
        bgMMSMS.Source = bgSource;
        bgMMSMS.ID = (int)MusicType.Respawn;
        bgMMSMS.Track = MMSoundManager.MMSoundManagerTracks.Music;
        bgMMSMS.Persistent = true;
    }

    public void OnMMEvent(CorgiEngineEvent _event)
    {
        switch (_event.EventType)
        {
            case CorgiEngineEventTypes.Pause:
                SwitchToUIMusic();
                break;
            case CorgiEngineEventTypes.UnPause:
                ReturnToGameMusic();
                break;
            case CorgiEngineEventTypes.PlayerDeath:
                PlayerDeathMusicHandler();
                break;
            case CorgiEngineEventTypes.Respawn:
                RespawnMusicHandler();
                break;
            case CorgiEngineEventTypes.LevelStart:
                    LevelStartMusicHandler();
                break;
            default:
                break;
        }
    }

    protected void SwitchToUIMusic()
    {
        // MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.Music);
        // MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.Sfx);
        // MMSoundManager.Instance.PlayTrack(MMSoundManager.MMSoundManagerTracks.UI);

        // MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PauseTrack, MMSoundManager.MMSoundManagerTracks.Music);
        // MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PauseTrack, MMSoundManager.MMSoundManagerTracks.Sfx);
        // MMSoundManagerTrackEvent.Trigger(MMSoundManagerTrackEventTypes.PlayTrack, MMSoundManager.MMSoundManagerTracks.UI);

        UIMusicFeedback.PlayFeedbacks();
    }

    protected void ReturnToGameMusic()
    {
        // MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.UI);
        // MMSoundManager.Instance.PlayTrack(MMSoundManager.MMSoundManagerTracks.Music);
        // MMSoundManager.Instance.StopTrack(MMSoundManager.MMSoundManagerTracks.Sfx);
        resumeBackgroundMusicFeedback.PlayFeedbacks();
    }

    protected void PlayerDeathMusicHandler()
    {
        Debug.Log("deathMusic");
        // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Background, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
        // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Death, 0.2f, 0, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));
        PlayerDeathMusicFeedback.PlayFeedbacks();
    }

    protected void RespawnMusicHandler()
    {
        Debug.Log("Respawn Music");
        PlayerRepawnMusicFeedback.PlayFeedbacks();
       // MMSoundManagerSoundFadeEvent.Trigger((int)MusicType.Respawn, 0.1f, 10.0f, new MMTweenType(MMTween.MMTweenCurve.EaseInOutElastic));

    }

    protected void LevelStartMusicHandler()
    {
       // MMSoundManager.Instance.PauseTrack(MMSoundManager.MMSoundManagerTracks.UI);
        backgroundMusicFeedback.PlayFeedbacks();
        Debug.Log("level started");
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }
}
