using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;


[CreateAssetMenu(fileName = "LevelMusic", menuName = "Scriptable Objects/LevelMusic", order = 1)]
public class LevelMusicSO : ScriptableObject
{


    //[SerializeField]
    //private MMFeedbacks backgroundMusicFeedback;
    //[SerializeField]
    //private MMFeedbacks resumeBackgroundMusicFeedback;
    //[SerializeField]
    //private MMFeedbacks bossMusicFeedback;
    //[SerializeField]
    //private MMFeedbacks UIMusicFeedback;
    //[SerializeField]
    //private MMFeedbacks PlayerDeathMusicFeedback;
    //[SerializeField]
    //private MMFeedbacks PlayerRepawnMusicFeedback;

    
    public AudioClip backgroundMusic;
 
    public AudioClip bossMusic;

    public AudioClip miniBossMusic;

    [HideInInspector]
    public MMSoundManagerSound backGroundMusicSound;
    [HideInInspector]
    public MMSoundManagerSound bossMusicSound;
    [HideInInspector]
    public MMSoundManagerSound miniBossMusicSound;

    private void Awake()
    {
        ConvertToSoundManagerSounds();
    }

    private void ConvertToSoundManagerSounds()
    {
        backGroundMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        backGroundMusicSound.ID = 1;
        backGroundMusicSound.Persistent = false;

        bossMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        bossMusicSound.ID = 2;
        bossMusicSound.Persistent = false;

        miniBossMusicSound.Track = MMSoundManager.MMSoundManagerTracks.Music;
        miniBossMusicSound.ID = 3;
        miniBossMusicSound.Persistent = false;
    }

}
