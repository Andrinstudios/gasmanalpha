﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


// This class is used to assign the player to the virtual camera if using a Cinemachine Camera system
public class GMCameraFollow : MonoBehaviour
{
    public GameObject player;
    public CinemachineVirtualCamera vCam;
    public CinemachineImpulseListener _impulseListener;

    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        SetCameraFollowToPlayer();
    }

    // Update is called once per frame
    void SetCameraFollowToPlayer()
    {
        if (player != null)
        {
            vCam.Follow = player.transform;
        }else
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        
    }
}
