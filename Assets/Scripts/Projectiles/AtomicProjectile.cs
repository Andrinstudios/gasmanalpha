﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{

    public class AtomicProjectile : CloudProjectile {

        public float playerBoostForce = 100f;
        private CorgiController _controller;

        protected override void Initialization()
        {
            base.Initialization();

            if (_owner != null)
            {
                _controller = _owner.GetComponent<CorgiController>();
              //  BoostCharacter();
            }
        }

        void BoostCharacter()
        {
           // _controller.SetVerticalForce(playerBoostForce);
            _controller.SetVerticalForce(Mathf.Sqrt(2f * playerBoostForce * Mathf.Abs(_controller.Parameters.Gravity)));

           // Animator anim = playerObject.GetComponent<Animator>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            GameObject colObject = collision.gameObject;
            if (colObject.tag == "Enemy")
            {
                Vector2 direction = collision.gameObject.transform.position - transform.position; 
                Rigidbody2D rb = colObject.GetComponent<Rigidbody2D>();
                Vector2 blastForce = new Vector2(500f * direction.x, 500f);
                rb.AddForce(blastForce);
            }
        }

    }
}
