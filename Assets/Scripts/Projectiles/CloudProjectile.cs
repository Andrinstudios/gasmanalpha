﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

[RequireComponent(typeof(Rigidbody2D))]

//TODO Fix isse where first cloud released does 10 damage rather than 1

public class CloudProjectile : Projectile
{
    protected Rigidbody2D _rigidBody2D;
    protected Vector2 _fartForce;

    [Header("Cloud Properties")]
    public CloudType _cloudType;

    [HideInInspector]
    public bool _forceApplied = false;
    public bool isFlammable = true;

    [HideInInspector]
    public bool isIgnited = false;
    
    [HideInInspector]
    public bool inVacuum = false;
   // [HideInInspector]
    public bool isConverted = false;


    public float gasUsedPerProjectile = 5f;

    [Header("Effects")]
    public List<ParticleSystem> particleEffects;
    public ParticleSystem gasParticles;

[Header("Ignition Parameters")]
    public ParticleSystem igniteEffect;
    public AudioClip explosionSound;
    public float delayBetweenIgnitions = 0.5f;
    private float masterDealyBetweenIgnitions;

    private bool isScented = false;
    private float origianlCloudDamage = 0;
    private float initialAlpha;
    public Material gasParticleMaterial;


    protected override void Initialization()
    {
        base.Initialization();
        _rigidBody2D = this.GetComponent<Rigidbody2D>();
        ActivateEffects(false);
        masterDealyBetweenIgnitions = delayBetweenIgnitions;
       _spriteRenderer.enabled = false;
       gasParticleMaterial = gasParticles.GetComponent<ParticleSystemRenderer>().sharedMaterial;
    }



    /// <summary>
    /// On enable, we reset the object's speed
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
        _forceApplied = false;
        PlayParticles();

    }

    protected override void Update()
    {
        base.Update();

        //set the scale of the gas particles to match that of the sprite/collider.
        gasParticles.transform.localScale = transform.localScale;
    }

    /// <summary>
    /// Handles the projectile's movement, every frame
    /// </summary>
    public override void Movement()
    {
        if (!_forceApplied && (Direction != Vector3.zero))
        {
            _fartForce = -Direction * Speed * Random.Range(0.9f, 1.1f);
            _rigidBody2D.AddForce(_fartForce);
            _forceApplied = true;
            RemoveGas();
        }
    }
    /// <summary>
    /// Calls the gasManager to update the gas level of the tank
    /// </summary>
    private void RemoveGas()
    {
        if (GasManager.Instance != null)
        {
            GasManager.Instance.RemoveGas(gasUsedPerProjectile);
            ActivateEffects(true);
        }
    }

    private void PlayParticles()
    {
        //be sure the renderer is on because it is set to off after ignition
        gasParticles.GetComponent<ParticleSystemRenderer>().enabled = true;

        gasParticles.Play();
    }

    private void ActivateEffects(bool status)
    {
        foreach (ParticleSystem system in particleEffects)
        {
            if (system == null) { return; }
            if (status)
            {
                system.Play();

            }else{
                system.Pause();
            }
        }
    }

    public void Ignite(bool isFlame)
    {
        if (isFlame)
        {
            delayBetweenIgnitions = 0.1f;
        }
        StartCoroutine("TriggerIgnition");
    }

     protected override void OnDisable()
    {
        DamageOnTouch _damage = GetComponentInChildren<DamageOnTouch>();  

        if (isScented)
        {
            isScented = false;
            _damage.MaxDamageCaused = origianlCloudDamage;
            _spriteRenderer.color = Color.white;
        }

        if (isIgnited){
            isIgnited = false;
            _spriteRenderer.color = Color.white;
            IgniteOnTouch _flame = GetComponentInChildren<IgniteOnTouch>();
            _flame.enabled = false;
            _flame.isFlame = false;
            DamageOwner = false;
            _spriteRenderer.enabled = true;

            _damage.MaxDamageCaused = origianlCloudDamage;
            _damage.TargetLayerMask = _damage.TargetLayerMask &= ~(1 << 9); //remove player from layermask
            delayBetweenIgnitions = masterDealyBetweenIgnitions;
        }

        if (inVacuum)
        {
            inVacuum = false;
            DamageOnTouch _damageOnTouch = GetComponentInChildren<DamageOnTouch>();
            _damageOnTouch.enabled = true;
        }

        if (isConverted)
        {
            gasParticles.GetComponent<ParticleSystemRenderer>().material = gasParticleMaterial;
            isConverted = false;
            DamageOnTouch _dot = GetComponentInChildren<DamageOnTouch>();
            _dot.TargetLayerMask = (1 << LayerMask.NameToLayer("Enemies"));

            ConvertGasToFlowersOnTouch cGas = GetComponentInChildren<ConvertGasToFlowersOnTouch>();
            cGas.enabled = false;

        }

        base.OnDisable();
    }
    public void StartConversion(Material flowerParticleMaterial)
    {
        StartCoroutine(TriggeFlowerConversion(flowerParticleMaterial));
    }


    //coroutine for coverting gas cloud to explosion
    IEnumerator TriggerIgnition()
    {
        if (isIgnited) 
        {
            yield return null;
        }

        isIgnited = true;
        yield return new WaitForSeconds(delayBetweenIgnitions);

        IgniteOnTouch _flame = GetComponentInChildren<IgniteOnTouch>();
        if (_flame != null)
        {
            _flame.enabled = true;
            _flame.isFlame = true;
        }
        else { Debug.Log("no ignite on touch found"); }
        

        //if this a sprite based cloud
       // _spriteRenderer.enabled = false;

        //if this is a particle based cloud
       
        var pRenderer = gasParticles.GetComponent<ParticleSystemRenderer>();
        pRenderer.enabled = false;

        DamageOwner = true;
        igniteEffect.Play();
        SoundManager.Instance.PlaySound(explosionSound,transform.position,false);
 
        DamageOnTouch _damage = GetComponentInChildren<DamageOnTouch>();
        _damage.TargetLayerMask = _damage.TargetLayerMask | (1 << 9); //add the player to layer mask
        origianlCloudDamage = _damage.MaxDamageCaused;
        _damage.MaxDamageCaused = _damage.MaxDamageCaused * 2;

        CancelInvoke();
        Invoke("Destroy", 1f);

    }

    IEnumerator TriggeFlowerConversion(Material _flowerMaterial)
    {
        if (isConverted)
        {
            yield return null;
        }

        isConverted = true;
        yield return new WaitForSeconds(delayBetweenIgnitions);

        ConvertGasToFlowersOnTouch _converter = GetComponentInChildren<ConvertGasToFlowersOnTouch>();
        _converter.enabled = true;
        Debug.Log("converted");
        /*  next steps
        *1.  change damage on touch collisionlayer to player only
        2.  change the sprite
        3.  change the particles
         */
        

        //store the original material then replace the particleRenderer material 
    
       // gasParticleMaterial = gasParticles.GetComponent<ParticleSystemRenderer>().material;
        gasParticles.GetComponent<ParticleSystemRenderer>().material = _flowerMaterial;

       // gasParticles.Stop();
       gasParticles.Simulate(0f,true,true,false);
       gasParticles.Play();

         //change the damage layer to player only so clouds damge the player now.
        DamageOnTouch _dot = GetComponentInChildren<DamageOnTouch>();
        _dot.TargetLayerMask = (1 << LayerMask.NameToLayer("Player"));
            
    }

    public void Potpurried()
    {
        Debug.Log("potpurried");
        if ( isScented == true)
        {
            return;
        }
        isScented = true;

        DamageOnTouch _dot = GetComponentInChildren<DamageOnTouch>();
        origianlCloudDamage = _dot.MaxDamageCaused;
        _dot.MaxDamageCaused = _dot.MaxDamageCaused/2;
        
        //TODO: change color of the cloud to indicate the change in damage/lifespan
        _spriteRenderer.color = Color.blue;

    }

    override public void Destroy()
    {
//        Debug.Log("material "+gasParticleMaterial.name);
        gasParticles.GetComponent<ParticleSystemRenderer>().material = gasParticleMaterial;
        base.Destroy();
    }

    
}

