﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AcidBombProjectile : Bomb, MMEventListener<CorgiEngineEvent>
{
    public LayerMask TargetLayerMask;
    public LayerMask WallMask;
    public LayerMask PlatformMask;
    public ParticleSystem damageAreaParticles;
    public ParticleSystem objectParticles;
    public ParticleSystem explosionParticles;
    public float areaWidth = 8f;
    
    public float downRayDistance = 6f;
    public float edgeRayDistance = 1f;
    public Transform damageAreaTransform;

    protected GMThrownObject gmThrownObject;
    protected bool isMovingRight = true;
    protected bool hasExploded = false;
    protected bool foundPlatform = false;
    protected bool foundWall = false;
    protected bool foundEdge = false;
    protected BoxCollider2D damageCollider;
    protected Vector3 originalColliderWidth;
    protected float wallRayDistance;
    protected float explosionArc;
    protected float explosionRotation;

   
    
    

    protected override void Initialization()
    {
        gmThrownObject = GetComponent<GMThrownObject>();
        base.Initialization();

        //store original damage collider size
        damageCollider = (BoxCollider2D)DamageAreaCollider;
        originalColliderWidth = damageCollider.size;
        wallRayDistance = damageCollider.size.x - 1;

        var psMain = damageAreaParticles.main;
        psMain.duration = DamageAreaActiveDuration - 1;
        

        //store original parameters for the emitter/shape
        var exPart = explosionParticles.shape;
        explosionArc = exPart.arc;
        explosionRotation = exPart.rotation.z;
      //  Debug.Log("init " + explosionRotation);
    }

    protected override void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasExploded)
        {
            return;
        }

        // if what we're colliding with isn't part of the target layers, we do nothing and exit
        if (!MMLayers.LayerInLayerMask(collision.gameObject.layer, TargetLayerMask))
        {
            return;
        }

//        Debug.Log("collided with " + collision.transform.name);

        //get the direction the object is moving for later.
        isMovingRight = GetDirection();

        Collider2D thisCollider = GetComponent<Collider2D>();
        thisCollider.enabled = false;

        FindPlatform();
        CheckForWalls();
        CheckForEdges();
        SetParticleSize();
        Explode();
    }

    //Cast ray downward to find nearest platform and get that point as start point for building damageArea and particles
    protected void FindPlatform()
    {
        //move the origin point back to avoid getting hung up on wall

        float xAdjust = this.transform.position.x;
        float constant = 0.3f;

        xAdjust = isMovingRight ? xAdjust - constant : xAdjust + constant;
        Vector3 rayOrigin = new Vector3(xAdjust, this.transform.position.y, this.transform.position.z);

        RaycastHit2D platformRay = MMDebug.RayCast(rayOrigin, Vector3.down, downRayDistance, PlatformMask, Color.red, true);

        if (platformRay)
        {
            foundPlatform = true;
            float pointX = platformRay.point.x;
            float pointY = platformRay.point.y;
            //move the damageArea to the platform
            damageAreaTransform.position = new Vector3(pointX, pointY, damageAreaTransform.position.z);
    
            //move the particles to the platform
            damageAreaParticles.transform.position = new Vector3(pointX, pointY, damageAreaTransform.position.z);
        }
        else
        {
            foundPlatform = false;
        }
        
    }

    protected void CheckForWalls()
    {
        //cast a ray in the direction the projectile is moving
        
        Vector2 rayVector;
        rayVector = isMovingRight ? Vector2.right : Vector2.left;

        RaycastHit2D wallRay = MMDebug.RayCast(this.transform.position, rayVector,wallRayDistance, WallMask, Color.red, true);

        if (wallRay)
        {
            foundWall = true;
            // SetDamageColliderOffset(wallRay.distance);
            SetDamageCollider(wallRay.distance);

            //change the angle of the explosion emitter
            if (wallRay.distance < 0.5f)
            {
                var explosionShape = explosionParticles.shape;
                float rot = isMovingRight ? explosionRotation + 90f : explosionRotation - 90f;
                explosionShape.rotation = new Vector3(0f, 0f, rot);
            }

        }
        else
        {
            // SetDamageColliderOffset(wallRayDistance);
            SetDamageCollider(0);
        }

    }

    // Look out from platform point in either direction to find holes
    protected void CheckForEdges()
    {
        float distanceToHole = 4;
        //cast a ray downward in succession from the middle to the edge of the collider
        //determine how many iterations to move from center.
        float rayInterval = 0.1f;
        float iterations = (damageCollider.size.x - 1) / rayInterval;
        
        Vector3 rayPostion = damageAreaTransform.transform.position;

        //shift the ray origin up from the collider edge to create some space and eliminate amiguous results.
        rayPostion.y = rayPostion.y + 0.2f;

        //looking forward
        for (int x = 0; x < iterations;x++)
        {
            rayPostion = new Vector3(rayPostion.x + rayInterval, rayPostion.y, rayPostion.z);
            RaycastHit2D holeRay = MMDebug.RayCast(rayPostion, Vector2.down, edgeRayDistance, PlatformMask, Color.red, true);

            if (!holeRay)
            {
                foundEdge = true;
              //  Debug.Log("found positve hole");
              //  Debug.Log("distance to hole = " + x * rayInterval);
                distanceToHole = x * rayInterval;
                SetDamageCollider(distanceToHole);
                
                break;
            }
        }


        //reset the postion of the ray back to center
        rayPostion = damageAreaTransform.transform.position;
        //shift the ray origin up from the collider edge to create some space and eliminate amiguous results.
        rayPostion.y = rayPostion.y + 0.2f;

        //looking back
        for (int x = 0; x < iterations; x++)
        {
            rayPostion = new Vector3(rayPostion.x - rayInterval, rayPostion.y, rayPostion.z);
            RaycastHit2D holeRay = MMDebug.RayCast(rayPostion, Vector2.down, edgeRayDistance, PlatformMask, Color.red, true);
            
            if (!holeRay)
            {
                foundEdge = true;
              //  Debug.Log("found negative hole");
              //  Debug.Log("distance to hole = " + x * rayInterval);
                distanceToHole = x * rayInterval;
                SetDamageCollider(distanceToHole);
                break;
            }
        }
      //  Debug.Log("end " + rayPostion);
      
    }

    protected void SetDamageCollider(float distance)
    {
        float width = damageCollider.size.x;
        float fixedWidth = 1;
        float xOffset;
        float colliderWidth;
        float yOffset = damageCollider.size.y / 2;

        if (foundEdge)
        {
            xOffset = ((distance + fixedWidth) / 2) - fixedWidth;
            colliderWidth = distance + fixedWidth;
            xOffset = isMovingRight ? xOffset : 0 - xOffset;
            yOffset = damageCollider.size.y / 2;

            damageCollider.offset = new Vector3(xOffset, yOffset, 0);

            Vector3 edgeDamageArea = new Vector3(colliderWidth, damageCollider.size.y, 0);
            damageCollider.size = edgeDamageArea;
            return;

        }


        if (!foundWall)
        {
            xOffset = (width / 2) - fixedWidth;
            colliderWidth = damageCollider.size.x;
        }
        else
        {
            xOffset = ((distance + fixedWidth) / 2) - fixedWidth;
            colliderWidth = distance + fixedWidth;
        }


        xOffset = isMovingRight ? xOffset : 0 - xOffset;
        

        damageCollider.offset = new Vector3(xOffset, yOffset, 0);

        Vector3 newDamageArea = new Vector3(colliderWidth, damageCollider.size.y, 0);
        damageCollider.size = newDamageArea;

    }

    //this sets the size of the damage collider and adjusts the offest to account for walls
    protected void SetDamageColliderOffset(float distance)
    {
       // Debug.Log("collidersize in =  " + damageCollider.size);
        float width = damageCollider.size.x;

       // float fixedWidth = width / 3;
        float fixedWidth = 1;

        float xOffset = ((distance + fixedWidth) / 2) - fixedWidth;
      //  Debug.Log("sizes: " + width +" "+ distance +" "+ fixedWidth);

        float yOffset = damageCollider.size.y / 2;

        xOffset = isMovingRight ? xOffset : 0 - xOffset;

        damageCollider.offset = new Vector3(xOffset, yOffset, 0);

        Vector3 newDamageArea = new Vector3(distance + fixedWidth, damageCollider.size.y, 0);
        damageCollider.size = newDamageArea;
       // Debug.Log("damage set to " + newDamageArea);
    }

    protected void SetParticleSize()
    {
        
        //reset the size of the emitter based on the size of the damageCollider
        var ps = damageAreaParticles.shape;
        ps.position = DamageAreaCollider.offset;
        ps.scale = damageCollider.size;
      

     //   Debug.Log("ps size = " + ps.scale);
     //   Debug.Log("ps position = " + ps.position);

        //adjust the eission rate to account for the decreased size of the emitter.
        var pe = damageAreaParticles.emission;
        pe.rateOverTime = new ParticleSystem.MinMaxCurve(pe.rateOverTime.constantMax * (damageCollider.size.x / originalColliderWidth.x));
        
    }

    protected bool GetDirection()
    {
        float value = gmThrownObject.Direction.x;
        if (value < 0)
        {
            return false;
        }

        return true;
    }

    protected void Explode()
    {

        hasExploded = true;

        //hide the sprite
        _renderer.enabled = false;

        objectParticles.Stop(false);

        //set the directin of the explosion if there is no wall
        if (!foundWall)
        {
            var explosionShape = explosionParticles.shape;
            float rot = isMovingRight ? explosionRotation : explosionRotation + 50f;
            explosionShape.rotation = new Vector3(0f, 0f, rot);
        }
        

        ExplosionFeedback?.PlayFeedbacks();

        //if we are out of range of the platform then do not acitvate effects and damage area.
        if (!foundPlatform)
        {
            StartCoroutine(NoDamageDestroy()); 
            return;
        }

        //turn on the damage area
        EnableDamageArea();


        damageAreaParticles.Play();
        var ps = damageAreaParticles.shape;
//        Debug.Log("playing size = " + ps.scale);

        // _damageAreaActive = true;

        //freeze the object so it does not fall through the platform.
        Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
        thisBody.isKinematic = true;
        thisBody.velocity = Vector3.zero;
        thisBody.angularVelocity = 0f;

        //start the destroy timer.
        StartCoroutine(DestroySequence());
    }

    IEnumerator DestroySequence()
    {
        foundWall = false;
        foundEdge = false;
        yield return new WaitForSeconds(DamageAreaActiveDuration);
        DisableDamageArea();
        damageCollider.size = originalColliderWidth;
        ExplosionFeedback?.StopFeedbacks();

        //reset the explosion shape to baseline.
        yield return new WaitForSeconds(1f);
        var explosionShape = explosionParticles.shape;
        explosionShape.rotation = new Vector3(0f, 0f, explosionRotation);
        
        Destroy();

       // ResetGrenade();
    }

    //call this when player dies and need to stop the feedbacks and reset the grenade.
    protected void DestroyNow()
    {
        Debug.Log("destry grenade now");
        foundEdge = false;
        foundWall = false;
        DisableDamageArea();
        damageCollider.size = originalColliderWidth;
        ExplosionFeedback?.StopFeedbacks();
        var explosionShape = explosionParticles.shape;
        explosionShape.rotation = new Vector3(0f, 0f, explosionRotation);

        damageAreaParticles.Stop();
        damageAreaParticles.gameObject.SetActive(false);
        damageAreaParticles.Clear();
        explosionParticles.Clear();
        objectParticles.Clear();
        

    }

    IEnumerator NoDamageDestroy()
    {
        yield return new WaitForSeconds(1.1f);
        var explosionShape = explosionParticles.shape;
        explosionShape.rotation = new Vector3(0f, 0f, explosionRotation);
        Destroy();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        hasExploded = false;
        objectParticles.Play(false);

        damageAreaParticles.gameObject.SetActive(true);

        Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
        thisBody.isKinematic = false;

        Collider2D _thisCollider = GetComponent<Collider2D>();
        _thisCollider.enabled = true;
        damageCollider.offset = new Vector3(0, 0, 0);

        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    private void ResetGrenade()
    {
        Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
        thisBody.isKinematic = false;

        //turn off collider
        Collider2D _thisCollider = GetComponent<Collider2D>();
        _thisCollider.enabled = true;

    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                DestroyNow();
                break;
            default:
                break;
        }
    }
}


