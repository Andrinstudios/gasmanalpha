﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class ConvertGasToFlowersOnTouch : MonoBehaviour
{
    public LayerMask _layerMask;
   // public bool isFlame = false;

    public Material flowerParticleMaterial;
    public AudioClip conversionSound;
    
	private void Start() {
		
	}
private void OnTriggerEnter2D(Collider2D other)
{
	if(!isActiveAndEnabled)
	{
		return;
	}

	//check to see if what we are colliding with is in the collision LayerMask
	int layer = other.gameObject.layer;
	if (_layerMask != (_layerMask | (1 << layer)))
	{
		return;
	}
	

	CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
	if (_cloud == null || _cloud.isIgnited)
	{
		return;
	}
	_cloud.StartConversion(flowerParticleMaterial);

}

private void OnTriggerStay2D(Collider2D other) {
	if(!isActiveAndEnabled)
	{
		return;
	}

	//check to see if what we are colliding with is in the collision LayerMask
	int layer = other.gameObject.layer;
	if (_layerMask != (_layerMask | (1 << layer)))
	{
		return;
	}

	CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
	
	if (_cloud == null || _cloud.isIgnited)
	{
		return;
	}

	if (_cloud.isConverted)
	{
		return;
	}
	_cloud.StartConversion(flowerParticleMaterial);

}
}
