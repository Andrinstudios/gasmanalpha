﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMThrownObject : ThrownObject
{
    //Subclasses the thrownobject class but overrides to allow for throwing in an arc.

	public override void SetDirection(Vector3 newDirection, Quaternion newRotation, bool spawnerIsFacingRight = true)
	{
		if (DirectionCanBeChangedBySpawner)
		{
			Direction = newDirection;
			//Direction = new Vector3(newDirection.x, Direction.y, Direction.z);
			Debug.Log("new D" + Direction);
		}
		if (ProjectileIsFacingRight != spawnerIsFacingRight)
		{
			Flip();
		}
		if (FaceDirection)
		{
			transform.rotation = newRotation;
		}
	}


}
