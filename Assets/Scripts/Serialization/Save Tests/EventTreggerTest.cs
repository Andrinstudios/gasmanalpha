using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class EventTreggerTest : MonoBehaviour
{
    public void TriggerSaveEvent()
    {
        // MMEventManager.TriggerEvent(new GMEvent( GMEventTypes.Save, null));
        ES3AutoSaveMgr.Current.Save();
    }

    public virtual void TriggerLoadEvent()  
    {
        // MMEventManager.TriggerEvent(new GMEvent(GMEventTypes.Load, null));
        ES3AutoSaveMgr.Current.Load();
    }
}
