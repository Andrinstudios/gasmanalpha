using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class TestingSaveLoad : MonoBehaviour, MMEventListener<GMEvent>
{
    
    // Update is called once per frame
    public void SaveThis()
    {
        ES3.Save(this.gameObject.name, this.gameObject);
    }

    public void LoadThis()
    {
        ES3.Load(this.gameObject.name, this.gameObject);
    }

    public void OnMMEvent(GMEvent gmEvent)
    {
        switch (gmEvent.EventType)
        {
            case GMEventTypes.Save:
            
                ES3.Save(this.gameObject.name, this.gameObject);
                break;
            case GMEventTypes.Load:
                ES3.Load(this.gameObject.name, this.gameObject);
                break;

            default:
                break;
        }
    }

    private void OnEnable()
    {
        this.MMEventStartListening<GMEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<GMEvent>();
    }

}
