﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ES3Types;
using ES3Internal;

public class SavePrefab : MonoBehaviour
{
    public string prefabID;

    private void Awake()
    {
        ES3.Load(prefabID);
    }

    private void OnDestroy()
    {
        ES3.Save(prefabID, this.gameObject);
    }
}
