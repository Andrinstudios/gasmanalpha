﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIShootOnSite_GM : AIShootOnSight {

	  protected override void  Update () 
		{
			if ( (_character == null) || (_characterShoot == null) ) { return; }

			if ((_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
				|| (_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Frozen))
			{
				_characterShoot.ShootStop();
				return;
			}

			// determine the direction of the raycast 
			_direction = (_character.IsFacingRight) ? transform.right : -transform.right;
						
			// we cast a ray in front of the agent to check for a Player
			_raycastOrigin.x = _character.IsFacingRight ? transform.position.x + RaycastOriginOffset.x : transform.position.x - RaycastOriginOffset.x;
			_raycastOrigin.y = transform.position.y + RaycastOriginOffset.y;
			_raycast = MMDebug.RayCast(_raycastOrigin,_direction,ShootDistance,TargetLayerMask,Color.yellow,true);

			// if the raycast has hit something in the layer mask set in the insepctor we check
			// what is is then shoot if it is the player - else nothing.
			// This allows player to hide behind objects
			if (_raycast)
			{
				if (_raycast.collider.gameObject.layer != 9){return;}
				_characterShoot.ShootStart();
			}
			// otherwise we stop shooting
			else
			{
				_characterShoot.ShootStop();
			}

			if (_characterShoot.CurrentWeapon != null)
			{
				if (_characterShoot.CurrentWeapon.GetComponent<WeaponAim>() != null)
				{
					Vector3 direction = LevelManager.Instance.Players [0].transform.position - this.transform.position;
					_characterShoot.CurrentWeapon.GetComponent<WeaponAim> ().SetCurrentAim (direction);
				}
			}
		}
}
