﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine.SceneManagement;



public class GMHealth : Health, MMEventListener<GMEvent>, Respawnable
{
    [MMReadOnly]
    public string objectID;
    [MMReadOnly]
    public string stateID;
    private bool shouldRespawn = true;

    private void Awake()
    {
       // Debug.Log("Vac awake");
        objectID = gameObject.scene.name + gameObject.name;
        stateID = gameObject.scene.name + gameObject.name + "State";

        //check to 
        if (SaveManager.Instance.ShouldReset(gameObject.scene.name))
        {
            shouldRespawn = true;
        }
    }

    protected override void Initialization()
    {
      //  Debug.Log("initializing " + gameObject.name);
        base.Initialization();
        ////Debug.Log("respawn = " + ES3.Load<bool>("Respawn", SaveManager.Instance.initSettings));
        ////check for respawn
        //if (!ES3.KeyExists("Respawn", SaveManager.Instance.initSettings)) { return; }


        //if (ES3.Load<bool>("Respawn", SaveManager.Instance.initSettings))
        //{
        //    //reset the file to load on next occuance then exit to prevent reload now.
        //    ES3.Save<string>(stateID, "Load", SaveManager.Instance.sceneSettings);
        //    return;
        //}
        if (shouldRespawn) { return; }

        string eLoadState = ES3.Load<string>(stateID, SaveManager.Instance.sceneSettings);
        switch (eLoadState)
        {
            case "Load":
                EnemyData _eData = new EnemyData();
                if (ES3.KeyExists(objectID, SaveManager.Instance.sceneSettings))
                {
                   // Debug.Log("Loading saved data in " + gameObject.scene.name);
                    _eData = ES3.Load<EnemyData>(objectID, _eData, SaveManager.Instance.sceneSettings);

                    CurrentHealth = _eData.health;
                    gameObject.transform.position = _eData.position;
                }
                break;

            case "Dead":
               // Debug.Log("dead case");
                Kill();
                break;

            default:
                break;
        }

        shouldRespawn = false;
        return;

        //TODO:  is all the following code needed.  Currently unreachable!!

        Debug.Log("initializing "+gameObject.name+"  "+transform.position);
        
        if (ES3.KeyExists(gameObject.scene.name, SaveManager.Instance.initSettings))
        {
            //the scene is in the file so we check to see if player has visited and load from save is it has.
            //if the key does not exist the scene will load the default scene.
            if (ES3.Load<bool>(gameObject.scene.name, SaveManager.Instance.initSettings))
            {
                EnemyData _eData = new EnemyData();
                if(ES3.KeyExists(objectID, SaveManager.Instance.sceneSettings))
                {
                    Debug.Log("Loading saved data in " + gameObject.scene.name);
                    _eData = ES3.Load<EnemyData>(objectID, _eData, SaveManager.Instance.sceneSettings);

                    CurrentHealth = _eData.health;
                    gameObject.transform.position = _eData.position;
                }
                
            }
        }
    }

    public virtual void OnPlayerRespawn(CheckPoint point, Character player)
    {
        // ES3.Save<string>(stateID, "Respawn", SaveManager.Instance.sceneSettings);
        Debug.Log("Respawnable call");
    }

    protected override void OnEnable()
    {
        Debug.Log("vac enabled)");
        base.OnEnable();
        this.MMEventStartListening<GMEvent>();

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        //  ES3.Save<int>(healthID, CurrentHealth, SaveManager.Instance.sceneSettings);

        EnemyData eData = new EnemyData();
        
        eData.position = transform.position;
        eData.health = CurrentHealth;

        Debug.Log("vac disabled)");

        this.MMEventStopListening<GMEvent>();

        MMEventManager.TriggerEvent(new GMDataEvent(GMDataEventTypes.Enemy, gameObject.scene.name, objectID, eData));
    }


    public virtual void OnMMEvent(GMEvent gEvent)
    {
        if (gEvent.SceneName == gameObject.scene.name)
        {
            switch (gEvent.EventType)
            {
                case GMEventTypes.SceneTriggerEnter:
                   // Debug.Log("scene entered" + gEvent.SceneName);
                    //  LoadEnemyData();
 
                    break;

                case GMEventTypes.SceneTriggerExit:
                  //  Debug.Log("scene exit" + gEvent.SceneName);
                    SaveEnemyData();
         
                    break;
                
                default:
                    break;

            }
        }
    }

    protected void LoadEnemyData()
    {
        EnemyData _eData = new EnemyData();
        _eData = ES3.Load<EnemyData>(objectID, _eData, SaveManager.Instance.sceneSettings);

        CurrentHealth = _eData.health;
        gameObject.transform.position = _eData.position;
    }

    protected void SaveEnemyData()
    {
       // Debug.Log("saved health " + CurrentHealth);

        EnemyData eData = new EnemyData();

        eData.health = CurrentHealth;
        eData.position = gameObject.transform.position;
     //   eData.isAlive = true;

        ES3.Save<EnemyData>(objectID, eData, SaveManager.Instance.sceneSettings);
        ES3.Save<string>(stateID, "Load", SaveManager.Instance.sceneSettings);
    }

    protected override void DestroyObject()
    {
      //  Debug.Log("vac destroy");
        base.DestroyObject();
        ES3.Save<string>(stateID, "Dead", SaveManager.Instance.sceneSettings);
        if(_autoRespawn != null)
        {
           // Debug.Log("auto is on");
        }

    }

    public override void Revive()
    {
        base.Revive();
       // Debug.Log("vac revive");
    }

}
