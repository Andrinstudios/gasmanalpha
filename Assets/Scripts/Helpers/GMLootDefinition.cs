using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GMLootDefinition", menuName = "GM/GMLoot Definition")]
public class GMLootDefinition : ScriptableObject
{
    public GMLootTable LootTable;

    public virtual GameObject GetLoot()
    {
        return LootTable.GetLoot()?.Loot;
    }

    /// <summary>
    /// computes the loot table's weights
    /// </summary>
    public virtual void ComputeWeights()
    {
        LootTable.ComputeWeights();
    }
}
