﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


[ES3Serializable]
public class EnemyData
{
    public float health;
    public Vector3 position;
    public bool wasInstantiated;
}

/* things this class needs to do.
 * controls the saving and loading of enemies/items in the scene
 * keep track of whether to respawn or reload the object
 * needs to keep track of inital parameters
 * 
 * Would this be easier with an interface "Saveable" in order to collect all the objects
 * and keep them in a central location rather than with the onjects themselves?
 */

public class GMSaveLoad : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{

    private Health _health;
    private SceneDataHolder _sceneData;

    public bool isInstanstiated = false;
    [MMReadOnly]
    public bool isRespawning = false;
    [SerializeField]
    private bool isFirstLoad = true;
    
    private bool playerDiedOffScreen = false;

    //  public string objectID;
    public EnemyData initialEnemyData;

    private AIBrain _brain;
    

    private void Awake()
    {
       // Debug.Log(gameObject.name + " is awake");
        _health = gameObject.GetComponent<Health>();
        if (_health == null)
        {
            _health = gameObject.GetComponentInChildren<Health>();
        }
        _sceneData = FindObjectOfType<SceneDataHolder>();

        _brain = GetComponent<AIBrain>();
        if (_brain == null)
        {
            _brain = GetComponentInChildren<AIBrain>();
        }


        //if this is an instantiated object we do not need to store initial info as it will not respawn
        //instead the object will simply be removed/inactivated for reinstantiation later.
        if (!isInstanstiated)
        {
            StoreInitialData();
        }
        else
        {
          //  Debug.Log(gameObject.name + " listening");
            this.MMEventStartListening<CorgiEngineEvent>();
        }
        
    }

    // register the object with sceneDataHolder to set respwan data.
    private void StoreInitialData()
    {
        if (gameObject.GetComponent<GMPickableItem>() != null) { return; }

       // Debug.Log("storing initial data for " + gameObject.name);
        EnemyData eData = new EnemyData();
        eData.health = _health.CurrentHealth;
        eData.position = this.transform.position;
        eData.wasInstantiated = isInstanstiated;

        _sceneData.AddToRespawnDictionary(this.gameObject.name, eData);
    }

    /// <summary>
    /// retreives data for reactivating from last known state
    /// </summary>
    private void ReloadData()
    {
       // Debug.Log("reloading "+gameObject.name); 
        EnemyData eData = new EnemyData();
        eData = _sceneData.LoadEnemyData(this.gameObject.name);
        if (eData != null)
        {
            //set the health to current health and update the the HealthBar if there is one (SetHealth)
            _health.CurrentHealth = eData.health;
            _health.SetHealth(_health.CurrentHealth, null);
            this.gameObject.transform.position = eData.position;
            
        }
        
    }

    
    /// <summary>
    /// used to reset the object to initial settings when player dies and respawns
    /// </summary>
    public void ResetData()
    {

        if (gameObject.GetComponent<GMPickableItem>() != null)
        {
            this.gameObject.SetActive(true);

            return;
        }

        // instantiated objects should not respawn but rather be cleared out.
        if (isInstanstiated)
        {
            //remove from proximity management
            GMProximityManager pManager = FindObjectOfType<GMProximityManager>();
            if(pManager != null)
            {
                pManager.RemoveManagedObject(gameObject.GetComponent<ProximityManaged>());
            }
            

            //deactivate the object
            this.gameObject.SetActive(false);
      
            return;
        }

        //Need to turn on the object in order to get the health compoent
        this.gameObject.SetActive(true);
        _health.Revive();

       // Debug.Log("Resetting "+this.gameObject.name);

        //create a data object to receive the data from the store and assign it to object
        EnemyData eData = new EnemyData();
        eData = _sceneData.ResetEnemyData(this.gameObject.name);
        _health.CurrentHealth = eData.health;
        this.gameObject.transform.position = eData.position;

        if (_brain != null)
        {
            _brain.ResetBrain();
        }
        

        //reset the healthbar if there is one.
        MMHealthBar bar = this.gameObject.GetComponent<MMHealthBar>();
        if (bar != null)
        {
            bar.Initialization();
        }


        ////TODO: check this for necessity.
        //if(isInstanstiated)
        //{
        //    this.gameObject.SetActive(false);
        //}
    }

    public void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
       // Debug.Log("enabled " + this.gameObject.name);

        //initial activation of the object so do nothing
        if (isFirstLoad)
        {
            isFirstLoad = false;
            return;
        }


        // reset the respawning flag for the object unless it is instantiated.
        if(isRespawning && !isInstanstiated)
        {
          //  Debug.Log("respawning " + gameObject.name);
            
            isRespawning = false;

            return;
        }
       // Debug.Log("here " + gameObject.name);

        //Do nothing for instantiated objects
        if (isRespawning && isInstanstiated && !playerDiedOffScreen)
        {
           // Debug.Log("met " + gameObject.name);
           // gameObject.SetActive(false);
            isRespawning = false;
            return;
        }

        // inactivate the instantiated object when player dies off screen
        if (playerDiedOffScreen && isInstanstiated && isRespawning)
        {
            gameObject.SetActive(false);
            playerDiedOffScreen = false;
            return;
        }

        //We have passed all other options so we reload.
        ReloadData();
    }

    public void OnDisable()
    {
       // Debug.Log("disabled " + gameObject.name);
       if (_health != null)
        {
            //create a data object and store it in the reload dictionary
            EnemyData eData = new EnemyData();
            eData.health = _health.CurrentHealth;
            eData.position = gameObject.transform.position;
            _sceneData.AddToReloadDictionary(this.gameObject.name, eData);

        }
        

        if (!isInstanstiated)
        {
            this.MMEventStopListening<CorgiEngineEvent>();
        }
        
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                if(GetComponent<ProximityManaged>() == null)
                {
                    isRespawning = true;
                    break;
                }

                if (!GetComponent<ProximityManaged>().DisabledByManager)
                {
                    ResetData();
                }
                else
                {
                    playerDiedOffScreen = true;
                }
                
                isRespawning = true;
                break;
            default:
                break;
        }
    }


}
