﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Sirenix.OdinInspector;




public class SceneDataHolder : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    [ShowInInspector]
    private Dictionary<string, EnemyData> reloadEnemyDictionary = new Dictionary<string, EnemyData>();
    [ShowInInspector]
    private Dictionary<string, EnemyData> respawnEnemyDictionary = new Dictionary<string, EnemyData>();

    [ReadOnly]
    public List<PickableItem> pickupList;
    [ReadOnly]
    public List<GMSaveLoad> objectList;

    private void Start()
    {
        GetSavableObjects();
    }

    protected void GetSavableObjects()
    {
        var items = FindObjectsOfType<GMSaveLoad>();
        foreach (GMSaveLoad saveable in items)
        {
           // Debug.Log("object list: " + saveable.gameObject.name);
            objectList.Add(saveable);
        }

        var picks = FindObjectsOfType<PickableItem>();
        foreach (PickableItem pickable in picks)
        {
            pickupList.Add(pickable);
        }
    }

    public void AddEnemyToSaveableList(GMSaveLoad saveable)
    {
        objectList.Add(saveable);
    }

    public EnemyData LoadEnemyData(string eID)
    {
        if (!reloadEnemyDictionary.ContainsKey(eID))
        {
            return null;
        }

        return reloadEnemyDictionary[eID];
    }


    public EnemyData ResetEnemyData(string eID)
    {
        return respawnEnemyDictionary[eID];
    }


    protected void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();

    }


    protected void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public void AddToReloadDictionary(string eID, EnemyData eData)
    {
        reloadEnemyDictionary[eID] = eData;
    }

    public void AddToRespawnDictionary(string eID, EnemyData eData)
    {
  
        respawnEnemyDictionary[eID] = eData;
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        if (thisEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            SetObjectsToRespawn();
            RestorePickableItems();
        }
    }

    //  cycles though the list of object and sets the respawn flag for proper handling in GMSaveLoad
    protected void SetObjectsToRespawn()
    {
        foreach (GMSaveLoad sceneObject in objectList)
        {
            if (!sceneObject.gameObject.activeInHierarchy)
            {
                sceneObject.isRespawning = true;
                sceneObject.ResetData();
            }
        }
    }

    // cycles through the pickups in the scene and resets them on Player Respawn
    protected void RestorePickableItems()
    {
        foreach (PickableItem item in pickupList)
        {
            item.gameObject.SetActive(true);
        }
    }

}
