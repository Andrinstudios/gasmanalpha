using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using UnityEngine;
using Cinemachine;
using UnityEngine.Playables;

public class BossActivator : CheckpointCameraSwitcher
{
    protected bool sceneActivated = false;
    protected GMCharacterSummonerBosz _summonerAbility;
    protected PlayableDirector director;
    public GameObject BossObject;
    public Character bossCharacter;
    protected bool timelineTriggered = false;

    private void Awake()
    {
        //get the director component attached to the trigger object
        director = GetComponent<PlayableDirector>();

        //register to listen for the directors events
        director.played += Director_Played;
        director.stopped += Director_Stopped;
    }

    private void Director_Played(PlayableDirector obj)
    {
        //use this section to pause the player
        LevelManager.Instance.Players[0].Freeze();
        
    }

    private void Director_Stopped(PlayableDirector obj)
    {
        //restart the player
        LevelManager.Instance.Players[0].UnFreeze();

        //this prevents undesired drift of Boss by settting movement to zero
        CorgiController _bossController = BossObject.GetComponent<CorgiController>();
        _bossController.SetForce(Vector2.zero);
        bossCharacter.gameObject.GetComponent<AIBrain>().BrainActive = true;
        bossCharacter.gameObject.GetComponent<AIBrain>().TimeInThisState = 0f;
    }

    protected override void Start()
    {
        _summonerAbility = bossCharacter.gameObject.GetComponent<GMCharacterSummonerBosz>();
        if (_summonerAbility == null)
        {
            Debug.Log("No boss ability");
        }
        base.Start();
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !timelineTriggered)
        {
            timelineTriggered = true;
            ActivateScene();
        }
        base.OnTriggerEnter2D(collision);
        
    }

    public virtual void ActivateScene()
    {
        //activate cutscene (if needed)
        //turn on boss
        //Instantiate(BossObject);

        //_summonerAbility = BossObject.GetComponentInChildren<GMCharacterSummonerBosz>();
        //bossCharacter = BossObject.GetComponentInChildren<Character>();

        SwitchCamera();
        _summonerAbility.isTriggered = true;
        
        sceneActivated = true;
        director.Play();
        

    }

    public override void OnMMEvent(CorgiEngineEvent ceEvent)
    {
        if(ceEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            Reset();
        }
    }

    protected override void Reset()
    {
        sceneActivated = false;
        timelineTriggered = false;
        bossCharacter.gameObject.GetComponent<AIBrain>().enabled = false;

        //reset the timeline by setting the time to zero
        //stopping the timeline if has not finsihed
        //evaluate the timeline to move pieces back to starting locations
        director.time = 0;
        director.Stop();
        director.Evaluate();

        base.Reset();
    }
}
