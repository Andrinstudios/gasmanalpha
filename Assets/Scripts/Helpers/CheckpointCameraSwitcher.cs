using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using UnityEngine;
using Cinemachine;

public class CheckpointCameraSwitcher : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public CinemachineVirtualCamera defaultCamera;
    public LayerMask playerMask;
    private CinemachineVirtualCamera previousCamera;

    private int low = 10;
    private int high = 15;
    private bool isTriggered = false;

    protected virtual void Start()
    {
        
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(isTriggered)
        { return; }

        if (MMLayers.LayerInLayerMask(collision.gameObject.layer, playerMask))
        {
            isTriggered = true;
            SwitchCamera();
        }
    }

    protected virtual void SwitchCamera()
    {
        var cmBrain = CinemachineCore.Instance.GetActiveBrain(0);
        ICinemachineCamera activeCamera = cmBrain.ActiveVirtualCamera;
        previousCamera = activeCamera as CinemachineVirtualCamera;


        defaultCamera.Priority = high;
        previousCamera.Priority = low;

    }

    protected virtual void Reset()
    {
        isTriggered = false;
    }

    protected virtual void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent ceEvent)
    {
        if (ceEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            Reset();
        }
    }

}
