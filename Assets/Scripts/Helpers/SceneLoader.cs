﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class SceneLoader : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    bool isLoaded;
    bool shouldLoad;
    Character _player;
    protected MMStateMachine<CharacterStates.CharacterConditions> _condition;

    // Start is called before the first frame update
    void Start()
    {
        Character[] characters = GameObject.FindObjectsOfType(typeof(Character)) as Character[];
        foreach (Character aCharacter in characters)
        {
            if(aCharacter.CompareTag("Player"))
            {
                _player = aCharacter;
                break;
            }
        }
                
        if (SceneManager.sceneCount > 0)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                //save a flag to set that player enetered this scene
                ES3.Save<bool>(scene.name, false, SaveManager.Instance.initSettings);
                if (scene.name == gameObject.name)
                {
                    isLoaded = true;
                }
            }
        }
    }

    protected void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    // Update is called once per frame
    void Update()
    {
        TriggerCheck();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Player"))
        {
            shouldLoad = true;

            //save a flag to set that player enetered this scene
          //  StartCoroutine(SetRespwnFlagAfterDelay());
            Debug.Log("enetering trigger for " + gameObject.name);

            //need to check if this is happening during respawn if so we do not call this.
            //so if currentCheckpoint.Scene.name is in the active scene list from SceneManager then do not call this.

            //if (SaveManager.Instance.RespawnControlDict.ContainsKey(this.gameObject.name))
            //{
            //    return;
            //}

            MMEventManager.TriggerEvent(new GMEvent(GMEventTypes.SceneTriggerEnter,gameObject.name));
            

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            shouldLoad = false;
            Debug.Log("Leaving trigger for " + gameObject.name);

            //need to find out if the is beign called from the scene we died in if so skip it.
            //if (SaveManager.Instance.RespawnControlDict.ContainsKey(this.gameObject.name))
            //{
            //    Debug.Log("player dead so returning.");
            //    return;
            //}
                
   
            MMEventManager.TriggerEvent(new GMEvent(GMEventTypes.SceneTriggerExit, gameObject.name));
            
        }
    }

    void TriggerCheck()
    {
        if (shouldLoad)
        {
            LoadScene();
        }
        else
        {
            UnloadScene();
        }
    }

    void LoadScene()
    {
        if(!isLoaded)
        {
            SceneManager.LoadSceneAsync(gameObject.name, LoadSceneMode.Additive);
            isLoaded = true;
        }
    }

    void UnloadScene()
    {
        if(isLoaded)
        {    
            SceneManager.UnloadSceneAsync(gameObject.name);
            isLoaded = false;
        }
    }

    private IEnumerator SetRespwnFlagAfterDelay()
    {
        yield return new WaitForSeconds(1f);
        ES3.Save<bool>("Respawn", false, SaveManager.Instance.initSettings);
    }


    public void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:

             //   Debug.Log("player died "+gameObject.name);

                break;

            case CorgiEngineEventTypes.Respawn:
             //   Debug.Log("player respawn "+ gameObject.name);

                break;

            default:
                break;
        }
    }
}
