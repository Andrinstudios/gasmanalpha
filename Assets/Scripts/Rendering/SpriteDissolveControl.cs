using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class SpriteDissolveControl : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{

    [SerializeField] private Material material;
    private float dissolveAmount;
    private bool isDissolving = false;
    
    public Color dissolveColor;

    private string dissolveParamName = "_DissolveAmount";
    private string dissolveColorParamName = "_DissolveColor";


    private void Update()
    {
        if (isDissolving)
        {
            dissolveAmount = Mathf.Clamp01(dissolveAmount + Time.deltaTime);
            if (dissolveAmount == 1)
            {
                dissolveAmount = dissolveAmount + 0.1f;
            }
            material.SetFloat(dissolveParamName, dissolveAmount);

        }else
        {
            dissolveAmount = Mathf.Clamp01(dissolveAmount - Time.deltaTime);
            material.SetFloat(dissolveParamName, dissolveAmount);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isDissolving = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isDissolving = false;
        }
    }

    public void Dissolve()
    {
        material.SetColor(dissolveColorParamName, dissolveColor);
        isDissolving = true;
    }

    public void Appear()
    {
        isDissolving = false;
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                Reset();

                break;
            default:
                break;
        }
    }

    protected virtual void Reset()
    {
        isDissolving = false;
        dissolveAmount = 0;
        material.SetFloat(dissolveParamName, dissolveAmount);
    }

}
