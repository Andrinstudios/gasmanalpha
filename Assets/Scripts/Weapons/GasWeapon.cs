using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public enum CloudType
{
    Standard = 0, Fire = 1, Ice = 2
}

public class GasWeapon : ProjectileWeapon, MMEventListener<PickableItemEvent>
{
    //the object poolers that hold different projetiles to use with same weapon
    [MMInspectorGroup("ProjectilePoolers", false, 24)]
    [Tooltip("Order is important 0 standard, 2 Fire, 3 Freeze")]
    public MMSimpleObjectPooler[] poolers;

    public override void Initialization()
    {
        base.Initialization();
        ObjectPooler = poolers[(int)CloudType.Standard];
    }

    protected override void ShootRequest()
    {
        // get the object bieng shot and find the gas used per shot be validate firing is possible
        GameObject pooledObject = ObjectPooler.GetPooledGameObject();
        CloudProjectile cloud = pooledObject.GetComponent<CloudProjectile>();

        if (cloud == null) { return; }

        float gasNeededToFire = cloud.gasUsedPerProjectile;

        if (GasManager.Instance.currentGasLevel > gasNeededToFire)
        {
            WeaponState.ChangeState(WeaponStates.WeaponUse);
        }
        else
        {
            Debug.Log("Out of Gas!!!");
            //TODO  augment this to play a sound and animation when gsa level is too low.
        }
    }

    //need a function that responds to and event calling for type of cloud
    //triggered from the pick up
    //resets itself to standard after a given period of time.

    protected void ResetPoolerToStandard()
    {
        ObjectPooler = poolers[(int)CloudType.Standard];
    }

    public void OnMMEvent(PickableItemEvent _event)
    {
        PickableItem pickedItem = _event.PickedItem;

        if (pickedItem.tag == "Fire")
        {
            ObjectPooler = poolers[(int)CloudType.Fire];
            StartCoroutine(CloudTimer());
            return;
        }

        if (pickedItem.tag == "Ice")
        {
            ObjectPooler = poolers[(int)CloudType.Ice];
            StartCoroutine(CloudTimer());
            return;
        }
    }

    IEnumerator CloudTimer()
    {
        yield return new WaitForSeconds(10);
        ObjectPooler = poolers[(int)CloudType.Standard];
    }
    

    private void OnEnable()
    {
        this.MMEventStartListening<PickableItemEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<PickableItemEvent>();
    }
}
