﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class StickyGrenade : Bomb
{
    public bool isSticky;
    public LayerMask stickyLayer;
    protected Transform _previousParent;


    protected override void Initialization()
    {
        base.Initialization();
        //store the current parent in order to reset later
        _previousParent = transform.parent;
    }

    /*  
        private void OnCollisionEnter2D(Collision2D collision)
        {

            //if the object we are colliding with is not in the mask we do nothing
            int layer = collision.gameObject.layer;
            if (stickyLayer != (stickyLayer | (1 << layer)))
            {
                return;
            }
            Debug.Log("collision: " + collision);
            // get the rigid body and stop it where is contacts
            Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
            thisBody.isKinematic = true;
            thisBody.velocity = Vector3.zero;
            thisBody.angularVelocity = 0f;
             Debug.Log("old parent " + transform.parent.name);

            //store the current parent in order to reset later
            _previousParent = transform.parent;
            //paernt to the struck object 
            transform.parent = collision.transform;

            Debug.Log("new parent " + transform.parent.name);

        }
    */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Collider2D _thisCollider = GetComponent<Collider2D>();
        if (_thisCollider.enabled == false)
        {
            return;
        }

        //if the object we are colliding with is not in the mask we do nothing
        int layer = collision.gameObject.layer;
        if (stickyLayer != (stickyLayer | (1 << layer)))
        {
            return;
        }

        // get the rigid body and stop it where is contacts
        Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
        thisBody.isKinematic = true;
        thisBody.velocity = Vector3.zero;
        thisBody.angularVelocity = 0f;
     //   Debug.Log("old parent " + transform.parent.name);


        //paernt to the struck object 
        transform.parent = collision.transform;

        //turn off collider
        
        _thisCollider.enabled = false;
        
    }

    protected override void Destroy()
    {
        _renderer.enabled = true;
        _renderer.material.color = _initialColor;
        if (_poolableObject != null)
        {
            ResetGrenade();
            _poolableObject.Destroy();
        }
        else
        {
            Destroy();
        }

    }

    private void ResetGrenade()
    {
        Rigidbody2D thisBody = GetComponent<Rigidbody2D>();
        thisBody.isKinematic = false;

        //paernt to the struck object 
        transform.parent = _previousParent;

        //turn off collider
        Collider2D _thisCollider = GetComponent<Collider2D>();
        _thisCollider.enabled = true;

    }

}
