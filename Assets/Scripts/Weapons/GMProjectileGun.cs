﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMProjectileGun : ProjectileWeapon
{
    public float amoutOfGasPerUse;



    protected override void WeaponUse()
    {
        if (amoutOfGasPerUse > GasManager.Instance.currentGasLevel)
        {
            GasManager.Instance.PlayEmptySFX();
            return;
        }

        GasManager.Instance.RemoveGas(amoutOfGasPerUse);
        base.WeaponUse();
    }
}
