﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;


/*TODO: x1: shoot from butt 
 *      x2. update UI
 *      3. trigger animation
 *      4. propel player forward
 *      5. Aimable when airborne
 *      */

public enum GMAmmoPickupToggleEventTypes
{
    on,
    off

}

public struct GMAmmoPickupToggleEvent
{
    public GMAmmoPickupToggleEventTypes EventType;
   
    public GMAmmoPickupToggleEvent(GMAmmoPickupToggleEventTypes eventType)
    {
        EventType = eventType;
    }

    static GMAmmoPickupToggleEvent e;
    public static void Trigger(GMAmmoPickupToggleEventTypes eventType)
    {
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}

public class GMPlayerProjectileGun : OverrideProjectileGun, MMEventListener<AmmoPickUpEvent>, MMEventListener<CorgiEngineEvent>
{

    public float gasPerUse = 5;
    public float chargeThreshold = 10f;


    protected bool gunIsActive = false;
    protected float currentAmmoLevel = 0;


    protected override void ShootRequest()
    {
        if (!gunIsActive)
        {
            WeaponReloadMMFeedback?.PlayFeedbacks();
            
            return;
        }

        if (GasManager.Instance.currentGasLevel > gasPerUse)
        {
            WeaponState.ChangeState(WeaponStates.WeaponUse);
            AmmoPickUpEvent.Trigger(null, AmmoPickupEventTypes.useAmmo);
        }
        else
        {
            Debug.Log("Out of Gas!!!");
            return;
            //TODO  augment this to play a sound and animation when gsas level is too low.
        }

        currentAmmoLevel = currentAmmoLevel - 1;

        CheckAmmoLevel();
    }



    private void OnEnable()
    {
        this.MMEventStartListening<AmmoPickUpEvent>();
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<AmmoPickUpEvent>();
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(AmmoPickUpEvent ammoEvent)
    {
        if (ammoEvent.AmmoPickup != null)
        {
            if (ammoEvent.EventType != AmmoPickupEventTypes.addAmmo) { return; }
            if (ammoEvent.AmmoPickup.GetComponent<GMAmmoPickup>()!= null && !gunIsActive)
            {
                currentAmmoLevel = currentAmmoLevel + 1;
              //  Debug.Log("ammo level: " + currentAmmoLevel);
                CheckAmmoLevel();
            }
        }
    }

    public void OnMMEvent(CorgiEngineEvent cEvent)
    {
        if (cEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            currentAmmoLevel = 0;
            gunIsActive = false;
            GMAmmoPickupToggleEvent.Trigger(GMAmmoPickupToggleEventTypes.on);
        }
    }

    //test to see if the gun is redy to switch into firing mode
    protected void CheckAmmoLevel()
    {
        if (currentAmmoLevel == chargeThreshold)
        {
            gunIsActive = true;
            GMAmmoPickupToggleEvent.Trigger(GMAmmoPickupToggleEventTypes.off);
           // Debug.Log("toggle off");
        }

        if (currentAmmoLevel == 0)
        {
            gunIsActive = false;
            GMAmmoPickupToggleEvent.Trigger(GMAmmoPickupToggleEventTypes.on);
           // Debug.Log("toggle on");
        }
    }


    //called from the pickable Ammo to see if it can be picked
    public bool CanPickUpAmmo()
    {
        if (gunIsActive)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // propells the player in the opposite direction of the projectile
    protected override void KickBack()
    {
        float x = 1;
        float y = 0;

        CorgiController _controller = Owner.GetComponent<CorgiController>();

        //get the direction the player is facing to set the force direction properly
        bool isFacingRight = Owner.IsFacingRight;
        float dir = isFacingRight ? 1:-1;

        //get the rotation angle from the weapon transform rotation.
        float rot = transform.rotation.eulerAngles.z;


        bool isUpDown = false;

        //we need to clamp the rotation to 8 directions 

        if (rot < 22.5 || rot > 337.5)  // 0 degrees, right/left
        {
            x = 1;
            y = 0;
            
        }else if(rot >=22.5 && rot <= 67.5) //45 degrees up
        {
            x = 1;
            y = 1;
        }else if (rot > 67.5 && rot <=107.5)  // 90 degrees (up)
        {
            x = 0;
            y = 1;
            isUpDown = true;
        }
        else if (rot > 247.5 && rot <= 292.5)  // 270 degrees (down)
        {
            x = 0;
            y = -1;
            isUpDown = true;
        }
        else if (rot > 292.5 && rot <= 337.5)  //315 degrees (45 degrees down)
        {
            x = 1;
            y = -1;
        }

        Vector2 kickBackForce;

        // if we are shooting at 90 or 270 we do not want to adjust the y component for facingDirection
        if (!isUpDown)
        {
            kickBackForce = new Vector2(kickBackPower * x * dir, kickBackPower * y * dir);
        }
        else
        {
            kickBackForce = new Vector2(kickBackPower * x, kickBackPower * y * dir);
        }
        // add the force to the player.
        _controller.AddForce(kickBackForce);

        //TODO  add call to update anmimator

    }

}
