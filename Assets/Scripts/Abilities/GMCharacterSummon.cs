using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


public enum SummonType
{
    Above,
    Below,
    Enemy
}

public class GMCharacterSummon : CharacterAbility, MMEventListener<CorgiEngineEvent>
{
    public bool triggerActivated = false;
    public SummonType _summonType;

    public GameObject fallingAttack;
    public GameObject groundAttack;
    public GameObject enemyPrefab;
    public LayerMask platformMask;
    public float rayDistance = 8;
    public float fallingRayDistance = 10;

  //  public float minTime = 3f;
  //  public float maxTime = 5f;

    protected Transform _playerTransform;
    protected Vector3 _platformPosition;
    protected Vector3 _fallingPosition;

    protected GameObject groundAttackInstance;

    protected GMEnemyTeleporter _teleporter;

    // [HideInInspector]
    public bool isTriggered = false;


    protected override void Initialization()
    {
        _teleporter = GetComponent<GMEnemyTeleporter>();
        base.Initialization();
    }

    public virtual void Summon(Transform playerTransform)
    {
        if (!AbilityPermitted)
        {
            return;
        }

        if (triggerActivated && !isTriggered)
        {
            return;
        }
        // do not allow summon to happen if not triggered (prevents continued summon after player death)
        if (!isTriggered)
        {
            return;
        }

        _playerTransform = playerTransform;

        switch (_summonType)
        {
            case SummonType.Above:
                SummonAbove();
                break;
            case SummonType.Below:
                SummonBelow();
                break;
            case SummonType.Enemy:
                SummonEnemy();
                break;
            default:
                break;
        }
    }

    protected virtual void SummonBelow()
    {
       // Debug.Log("Summoning Below "+Time.time);
        /*  1. cast rays from playerLocation to find platform and edges
         *  2. Instantiate prefab to fit
         *  3. Apply damage
         *  4. Apply upward force to player
         *  5. destroy the prefab
         */

        if (FindPlatform())
        {
           groundAttackInstance = Instantiate(groundAttack, _platformPosition, Quaternion.identity);
        }
        else { return; }

      //  AbilityPermitted = false;
      //  StartCoroutine(SummonTimer());
    }

    protected virtual void SummonAbove()
    {
        if (CheckAbove())
        {
           Instantiate(fallingAttack, _fallingPosition, Quaternion.identity);
        }
        else { return; }

      //  AbilityPermitted = false;
      //  StartCoroutine(SummonTimer());
    }

    protected virtual void SummonEnemy()
    {

    }

    //protected virtual IEnumerator SummonTimer()
    //{
    //    float randomTime = Random.Range(minTime, maxTime);
    //    yield return new WaitForSeconds(randomTime);
    //    AbilityPermitted = true;
    //}

    protected bool FindPlatform()
    {
       if (_playerTransform == null)
        {
            _playerTransform = LevelManager.Instance.Players[0].transform;
        }


        RaycastHit2D platformRay = MMDebug.RayCast(_playerTransform.position, Vector3.down, rayDistance, platformMask, Color.red, true);

        if (platformRay)
        {
            _platformPosition = platformRay.point;
            return true;
        }

        return false;
    }

    protected bool CheckAbove()
    {

        RaycastHit2D platformRay = MMDebug.RayCast(_playerTransform.position, Vector3.up, fallingRayDistance, platformMask, Color.red, true);

        if (!platformRay)
        {
            //get the position of the top edge of the screen to place object.
            Vector3 screenEdge = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
            _fallingPosition = new Vector3(_playerTransform.position.x, screenEdge.y - 2, 0);
            return true;
        }

        return false;
    }

    public virtual void Trigger()
    {
        isTriggered = true;
    }

    protected virtual void OnTeleport()
    {
        StopAllCoroutines();
    }


    protected override void OnEnable()
    {
        Debug.Log("boss enabled");
        this.MMEventStartListening<CorgiEngineEvent>();
        if (_teleporter != null)
        {
            _teleporter.OnTeleport += OnTeleport;
        }

        base.OnEnable();
    }

    protected override void OnDisable()
    {
        Debug.Log("boss disabled");
        this.MMEventStopListening<CorgiEngineEvent>();
        
        if (_teleporter != null)
        {
            _teleporter.OnTeleport -= OnTeleport;
        }
        base.OnDisable();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                Reset();
              
                break;
            default:
                break;
        }
    }

    protected virtual void Reset()
    {
        isTriggered = false;
        AbilityPermitted = true;
    }
}
