﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
public class GMCharacterDash : CharacterDash
{
    /// amount of gas to remove per use of ability
    public float amountOfGasPerDash;
 

    public override void StartDash()
    {
        //Check to see if there is enough gas left in the tank
        if (amountOfGasPerDash > GasManager.Instance.currentGasLevel)
        {
            // play a feedback to let the player know your empty
            GasManager.Instance.PlayEmptySFX();

            return;
        }

        base.StartDash();
        GasManager.Instance.RemoveGas(amountOfGasPerDash);
    }



}
