﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class CharacterAtomic : CharacterAbility, MMEventListener<MMDamageTakenEvent>
{
    public float chargeTime = 3f;
    public float gasUsed = 100f;
    public float blastForce = 50f;
    public float blastRadius = 5f;
    public float damageForce = 10f;


    public SpecialAttack.Attack _attackType;

    [Header("Effects")]
    /// a list of effects to trigger when the weapon is used
    public List<ParticleSystem> ParticleEffects;

    private float amountCharged = 0;

    private float startTime = 0f;
    private float _currentGasLevel;


    private GasManager _gasManager;
    private bool hasBlasted = false;
    private bool chargingStarted = false;

    private Dictionary<string, bool> abilityDict;

    protected const string _chargingAnimationParameterName = "Charging";
    protected const string _jumpingAnimationParameterName = "Jumping";
    protected const string _idleAnimationParameterName = "Idle";
    protected int _chargingAnimationParameter;
    protected int _jumpingAnimationParameter;
    protected int _idleAnimationParameter;


  //  protected GMInputManager _gInputManager;
    protected GMInputSystemManager _gInputManager;

    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening<MMDamageTakenEvent>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening<MMDamageTakenEvent>();
    }

    public virtual void OnMMEvent(MMDamageTakenEvent damageEvent)
    {   
        if (damageEvent.AffectedCharacter == null)
        {
            return;
        }
        if (damageEvent.AffectedCharacter.CharacterType != Character.CharacterTypes.Player)
        {
            return;
        }

        if (!chargingStarted)
        {
            return;
        }

        ChargeStop();

    }

    protected override void Initialization()
    {
        base.Initialization();

        // _gInputManager = GMInputManager.Instance as GMInputManager;
        _gInputManager = GMInputSystemManager.Instance as GMInputSystemManager;

        abilityDict = new Dictionary<string, bool>();

        if (GasManager.Instance != null)
        {
            _gasManager = GasManager.Instance;
        }
        else { Debug.Log("no gas manager present in scene"); }
    }

    public override void ProcessAbility()
    {
        base.ProcessAbility();
    }

    protected override void HandleInput()
    {

        if (_gInputManager.ModifyFireButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
        {
            Debug.Log("modiy fire pressed");
        }

        if (_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonDown
           && _gInputManager.ModifyFireButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed)
        {
            ChargeStart();
        }

        if (_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed
            && _gInputManager.ModifyFireButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed)
        {
            ContinueCharge();
        }

        if (_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonUp
            || _gInputManager.ModifyFireButton.State.CurrentState == MMInput.ButtonStates.ButtonUp)
        {
            ChargeStop();
        }

        if (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown)
        {
            ChargeStart();
        }

        if (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed)
        {
            ContinueCharge();
        }

        if (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp)
        {
            ChargeStop();
        }

    }


    /// <summary>
    /// starts charing the abilty tank by draining gas tank
    /// </summary>
    private void ChargeStart()
    {

        //initialize the ability Dictionary to prep for a new pass through
        abilityDict = new Dictionary<string, bool>();
        amountCharged = 0f;

        //Check for conditions to allow charging to start
        if (_gasManager.currentGasLevel < gasUsed) { return; }
        if (!_controller.State.IsGrounded || _condition.CurrentState != CharacterStates.CharacterConditions.Normal) { return; }
        if (_movement.CurrentState == CharacterStates.MovementStates.Crouching
            || _movement.CurrentState == CharacterStates.MovementStates.Crawling) { return; }
        if (!AbilityPermitted) { return; }

        //Stop the character if it is moving
        _characterHorizontalMovement.MovementSpeed = 0f;

        //set flags and initial conditions
        chargingStarted = true;
        startTime = 0f;
        hasBlasted = false;
        _currentGasLevel = _gasManager.currentGasLevel;

        _character.MovementState.ChangeState(CharacterStates.MovementStates.Charging);

        //Turn off other abilities while charging.
        PauseAbilities();

    }

    private void ContinueCharge()
    {
        if (!_controller.State.IsGrounded)
        {
            return;
        }
        if (!chargingStarted)
        {
            return;
        }


        if (startTime < chargeTime)
        {
            startTime += Time.deltaTime;
            _gasManager.RemoveGas(gasUsed / chargeTime * Time.deltaTime);
            amountCharged += gasUsed / chargeTime * Time.deltaTime;


            PlayAbilityStartFeedbacks();


        }
        else
        {
            BlastOff();
            StopStartFeedbacks();
            RestoreAbilties();
        }
    }

    /// <summary>
    /// Stops charging if button released before timer elapsed
    /// </summary>
    private void ChargeStop()
    {
        _characterHorizontalMovement.MovementSpeed = _characterHorizontalMovement.WalkSpeed;
        if (!chargingStarted) { return; }

        if (!hasBlasted)
        {
            Debug.Log("amoutn charged " + amountCharged);
            _gasManager.AddGas(amountCharged);
            amountCharged = 0f;

            //_character.UnFreeze();
            StopStartFeedbacks();
        }
        StopStartFeedbacks();

        _character.MovementState.ChangeState(CharacterStates.MovementStates.Idle);

        RestoreAbilties();
        chargingStarted = false;
    }

    /// <summary>
    /// triigers abailty and depetes ability tank
    /// </summary>
    private void BlastOff()
    {
        if (!hasBlasted)
        {
            _controller.AddVerticalForce(blastForce);
             _character.MovementState.ChangeState(CharacterStates.MovementStates.Jumping);
            PlayAbilityStopFeedbacks();
            SetParticleEffects(true);
            ApplyDamage();
            hasBlasted = true;
            chargingStarted = false;
        }
        _characterHorizontalMovement.MovementSpeed = _characterHorizontalMovement.WalkSpeed;
        RestoreAbilties();
    }

    private void PauseAbilities()
    {
        //  Debug.Log("Pause");
        CharacterAbility[] _characterAbilities = GetComponents<CharacterAbility>();
        //  Debug.Log("Abiltiy count " + _characterAbilities.Length);


        foreach (CharacterAbility ability in _characterAbilities)
        {
            string abilityName = ability.GetType().Name;
           
            //store the state of each ability so we can restore them when through.
            abilityDict.Add(ability.GetType().Name, ability.AbilityPermitted);

            if (abilityName != _characterHorizontalMovement.GetType().Name)
            {
                ability.AbilityPermitted = false;
            }
           // Debug.Log("ability name: " + ability.GetType().Name + "  " + ability.AbilityPermitted);
        }
    }

    protected void RestoreAbilties()
    {
        CharacterAbility[] _characterAbilities = GetComponents<CharacterAbility>();

        foreach (CharacterAbility ability in _characterAbilities)
        {
            ability.AbilityPermitted = abilityDict[ability.GetType().Name];
            //  Debug.Log(ability.GetType().Name + " " + "restored");

        }
    }


    protected override void InitializeAnimatorParameters()
    {

        RegisterAnimatorParameter("Charging", AnimatorControllerParameterType.Bool,out _chargingAnimationParameter);
    }

    public override void UpdateAnimator()
    {
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _chargingAnimationParameter, (_movement.CurrentState == CharacterStates.MovementStates.Charging), _character._animatorParameters);
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _jumpingAnimationParameter, (_movement.CurrentState == CharacterStates.MovementStates.Jumping), _character._animatorParameters);
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _idleAnimationParameter, (_movement.CurrentState == CharacterStates.MovementStates.Idle), _character._animatorParameters);
    }


    private void SetParticleEffects(bool status)
    {
        foreach (ParticleSystem system in ParticleEffects)
        {
            if (system == null) { return; }

            if (status)
            {
                system.Play();
            }
            else
            {
                system.Pause();
            }
        }
    }

    private void ApplyDamage()
    {
        /*  two options
         * 1 - get a list of all enemies in the scence and apply damage based on distance
         * 2 - generate colliders at runtime and check for colision then apply damage.
         */

        //Option 1:
        Character[] _enemyList = FindObjectsOfType<Character>();

        foreach (Character enemy in _enemyList)
        {
            if (enemy.name != _character.name)
            {
                Vector2 distance = enemy.transform.position - _character.transform.position;
                float distFloat = distance.magnitude;
                Debug.Log("Enemy Distance" + enemy.name + "  " + distFloat);

                if (distFloat < blastRadius)
                {
                    //need to disable horizontal controller
                    CharacterHorizontalMovement cMove = enemy.GetComponent<CharacterHorizontalMovement>();
                    cMove.enabled = false;
                    Debug.Log("can't Move");

                    //get controller and add force to it for blast effect
                    CorgiController enemyController = enemy.GetComponent<CorgiController>();
                    Vector2 force = new Vector2(damageForce * Mathf.Sign(distance.x), damageForce);
                    enemyController.AddForce(force);
                     
                    KillEnemy(enemy);
                }
            }
        }
    }


    private void KillEnemy(Character _enemy)
    {
        EnemyHealth eHealth = _enemy.GetComponent<EnemyHealth>();

        eHealth.ApplySpecialDamage(10, _attackType);  

    }
 }



