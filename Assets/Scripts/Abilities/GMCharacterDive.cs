﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class GMCharacterDive :CharacterDive
{
	public float gasPerDive = 5;
	public float smashDamage;
	public float damageRadius;
	public float directHitMuliplier = 2;
	public float directHitDistance = 0.25f;
	public float stunDuration =1.5f;
/// <summary>
/// the amount of knockback forcer to apply to enemy
/// </summary>
	public float damageForce;
	public float OneWayPlatformDiveDealy=0.05f;
	public float OneWayPlatformsJumpCollisionOffDuration = 0.25f;
	public float InvulnerabilityTime = 1;


	public LayerMask DamageMask = 0;
	public float coolDownTime = 5f;
	private List<Health> enemyHealthList;
	
	private bool isInCooldown = false;



	protected override void Start()
	{
		enemyHealthList = new List<Health>();
		base.Start();
	}

	protected override void HandleInput()
	{
		if (isInCooldown){return;}
		
		if ((_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
			&& (_verticalInput < -_inputManager.Threshold.y))
		{
			// we start the dive coroutine
			InitiateDive();
			
		}
	}

    protected override void InitiateDive()
	{	
		//check to see if we are on a one way platform
		bool onAOneWayPlatform = (_controller.OneWayPlatformMask.MMContains(_controller.StandingOn.layer)
			|| _controller.MovingOneWayPlatformMask.MMContains(_controller.StandingOn.layer));

		if (!_controller.State.IsGrounded)
        {
			onAOneWayPlatform = false;
		}

		if ( !AbilityPermitted // if the ability is not permitted
			|| (_controller.State.IsGrounded && !onAOneWayPlatform) // or if the character is grounded
			|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping) // or if it's gripping
			|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)) // or if we're not under normal conditions
			
		{
			// we do nothing and exit  
			return;
		}

		// check for adequate gas in the private void OnFailedToConnectToMasterServer(NetworkConnectionError error)
		if (gasPerDive > GasManager.Instance.currentGasLevel)
		{
			//play empty sound effect for feedback
			GasManager.Instance.PlayEmptySFX();
			return;
		}else
		{
			GasManager.Instance.RemoveGas(gasPerDive);
			ExpellGas();
		}

		if (onAOneWayPlatform)
		{
			// we make it fall down below the platform by moving it just below the platform
			_controller.transform.position=new Vector2(transform.position.x,transform.position.y-0.1f);
			// we turn the boxcollider off for a few milliseconds, so the character doesn't get stuck mid platform
			StartCoroutine(_controller.DisableCollisionsWithOneWayPlatforms(OneWayPlatformsJumpCollisionOffDuration));
			//_controller.DetachFromMovingPlatform();
			//now launch a coroutine with a breif wait before starting the DIVE.
			StartCoroutine(PlatformDive());
			UITimerManager.Instance.StartCoroutine(UITimerManager.Instance.ReturnTest(coolDownTime, testValue => { isInCooldown = testValue; }));
			isInCooldown = true;
		}
		else
		{	// we start the dive coroutine
			StartCoroutine(Dive());
			UITimerManager.Instance.StartCoroutine(UITimerManager.Instance.ReturnTest(coolDownTime, testValue => { isInCooldown = testValue; }));
			isInCooldown = true;
		}
	}

	private void CastRaysRight()
	{
		// cast a ray to the right and store results in array
		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.right, damageRadius, DamageMask);
		foreach (RaycastHit2D hit in hits)
		{
			Health enemyHealth = hit.transform.GetComponent<Health>();
			if (enemyHealth != null)
            {
				if (!enemyHealthList.Contains(enemyHealth))
				{
					enemyHealthList.Add(enemyHealth);
				}
			}
		}
	}

	private void CastRaysLeft()
	{
		// cast a ray to the left and store results in array
		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.left,damageRadius, DamageMask);
		foreach (RaycastHit2D hit in hits)
		{
			Health enemyHealth = hit.transform.GetComponent<Health>();
			if (enemyHealth != null)
			{
				if(!enemyHealthList.Contains(enemyHealth))
                {
					enemyHealthList.Add(enemyHealth);
				}
				
			}

		}
	}
	// cast rays down to check for a direct hit and stun the enemy if there is.
	private void CastRaysDown()
	{
		RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.down, 5f, DamageMask);
		foreach (RaycastHit2D hit in hits)
		{
			//check to see if this is an enemy by looking Character Component
			Character hitCharacter = hit.transform.GetComponent<Character>();
			if (hitCharacter != null)
			{
				DamageOnTouch dot = hit.transform.GetComponent<DamageOnTouch>();
				if (dot != null)
				{
					dot.enabled = false;
					StartCoroutine(DOTTimer(dot));
				}
			}

			Health enemyHealth = hit.transform.GetComponent<Health>();
			if (enemyHealth != null)
            {
				//turn off the damageOnTouch for a direct hit to protect the player
				DamageOnTouch dot = hit.transform.GetComponent<DamageOnTouch>();
				if (dot != null)
				{
					dot.enabled = false;
					StartCoroutine(DOTTimer(dot));
				}
			}
		}

	}

	IEnumerator DOTTimer(DamageOnTouch _dot)
	{	
		yield return new WaitForSeconds(stunDuration);
		_dot.enabled = true;
	}

	IEnumerator PlatformDive()
	{
		yield return new WaitForSeconds(OneWayPlatformDiveDealy);
		StartCoroutine(Dive());
	}

	/// <summary>
	/// Coroutine used to make the player dive vertically
	/// </summary>
	protected override IEnumerator Dive()
	{
		// check for enemmies below
		CastRaysDown();

		//set the player to invulnerable while it is diving.
		_health.Invulnerable = true;

		// we start our sounds
		PlayAbilityStartFeedbacks();
		// PlayAbilityUsedSfx();

		// we make sure collisions are on
		_controller.CollisionsOn();
		// we set our current state to Diving
		_movement.ChangeState(CharacterStates.MovementStates.Diving);

		// while the player is not grounded, we force it to go down fast
		while (!_controller.State.IsGrounded)
		{
			_controller.SetVerticalForce(-Mathf.Abs(_controller.Parameters.Gravity)*DiveAcceleration);
			yield return null; //go to next frame
		}
		
		//Cast rays to find any Enemies
		CastRaysRight();
		CastRaysLeft();

		//out of the routine so player must be grounded - call the damage rouitine.
		//ApplyDamage();
		if(enemyHealthList.Count > 0)
		{
			ApplyDamageToHitList();
		}

		//staring the invulneralble timer
		StartCoroutine(InvulnerableTimer());

		// we play our exit sound
		StopStartFeedbacks();
		PlayAbilityStopFeedbacks();

		_movement.ChangeState(CharacterStates.MovementStates.Idle);
	}

	private IEnumerator InvulnerableTimer()
	{
		yield return new WaitForSeconds(InvulnerabilityTime);
		_health.Invulnerable = false;
	}

	private void ApplyDamageToHitList()
	{
		//foreach (Character _enemy in enemyHitList)
		foreach (Health eHealth in enemyHealthList)
			{

			Vector2 distance = eHealth.transform.position - _character.transform.position;
            float distFloat = distance.magnitude;
			
			// if the enemy is too far away skip to the next one.
			if (distFloat > damageRadius)
			{
				continue;
			}

			//	float damagePercent = (damageRadius - distFloat)/damageRadius;

			// calculate damage applied based on distance.
			//	float damageApplied = damagePercent * smashDamage;
			float damageApplied = smashDamage;
			Debug.Log(eHealth.name + " " + distFloat + " " + damageApplied);

			//Get the Health component to apply the damage
			//Health eHealth = _enemy.GetComponent<Health>();
			if (eHealth != null)
			{
				//check for a direct hit
				if(distFloat < directHitDistance)
				{
					damageApplied = smashDamage * directHitMuliplier;
				}
				//apply the damage
				if(damageApplied >= 1)
				{
					eHealth.Damage((int)damageApplied,_character.gameObject,stunDuration,stunDuration, new Vector3(0,-1,0));

					//Stun the enemy
					//_enemy.Freeze();
					Character _enemy = eHealth.GetComponent<Character>();
					if (_enemy != null)
                    {
						StartCoroutine(StartStunTimer(_enemy));
						//TODO: set up animation for stun
					}
					//get controller and add force to it for blast effect
					CorgiController enemyController = eHealth.GetComponent<CorgiController>();
					if (enemyController != null)
                    {
						Vector2 force = new Vector2(damageForce * Mathf.Sign(distance.x), damageForce);
						enemyController.AddForce(force);
                    }
                    
				}
			}
		}

		// reset the array for the next frame.
		enemyHealthList.Clear();
	}


	IEnumerator StartStunTimer(Character _enemy)
	{	
		_enemy.Freeze();
		yield return new WaitForSeconds(stunDuration);
		_enemy.UnFreeze();
		
		//TODO: change animation state back to previous or attack.


	}

	void ExpellGas()
	{
		Debug.Log("expell gas");
		//play a visual and sound effect

	}

}
