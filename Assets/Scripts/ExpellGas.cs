﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpellGas : MonoBehaviour {


    public int numberOfParticles = 10;      //number of gas sprites to generate
    public GameObject gasCloudPrefab;       //Prefab for the gas sprite
    public float partPerSecond = 10f;       //How may gas Sprites per second
    public float gasLifespan = 5;
    public float speed = 50;
    public float xOffset = 1;
    public float yOffset = -0.1f;
    float interval;                         //private float for InvokeRepeating rate
    int counter = 0;                        //intermedate for control

	// Use this for initialization
	void Start () {
        interval = 1 / partPerSecond;
        Debug.Log(interval);
        counter = numberOfParticles;
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("p"))
        {   Debug.Log(Time.time);
            InvokeRepeating("Expell", 0f, interval);
        }
        if (counter < 1)
        { 
            CancelInvoke("Expell");
            counter = numberOfParticles;
            Debug.Log(Time.time);
        }
	}

    void Expell()
    {
        Rigidbody2D prb = GetComponent<Rigidbody2D>();
        Vector2 playerVelocity = prb.velocity;

        float facing = 1f;
        //get the player facing direction to control gas direction  
        bool flipX = gameObject.GetComponent<SpriteRenderer>().flipX;
        if (flipX)
        {
            facing = -1f;
        }
        else { facing = 1f; }

        //set the postion for intantiation of the gas objects
        Vector3 currentLoc = transform.position;
        Vector3 spawnLoc = new Vector3(transform.position.x + (-facing * xOffset), transform.position.y + yOffset, transform.position.z);


        GameObject gasCloud = (GameObject)Instantiate(gasCloudPrefab, spawnLoc, Quaternion.identity);
        gasCloud.layer = 26;   // set the layer to the GasCloud Layer to avoid collision with the player.


        //get cloud rigidbody and randomize the force
        Rigidbody2D rb = gasCloud.GetComponent<Rigidbody2D>();
        Vector2 forceVector = new Vector2(-1 * facing * speed, Random.Range(0f,100f));
        rb.AddForce(forceVector);

        rb.drag = Random.Range(0.75f, 1f) * rb.drag;

        //vary the scale of the gasCloud
        Transform tr = gasCloud.transform;
        tr.localScale = Random.Range(0.5f, 1.1f) * tr.localScale;

        StartCoroutine(Lifespan(gasCloud));
        counter--;
    }


    //removes object from scene after designated time
    IEnumerator Lifespan(GameObject cloud)
    {
        yield return new WaitForSeconds(gasLifespan);
        Destroy(cloud);
    }
}
