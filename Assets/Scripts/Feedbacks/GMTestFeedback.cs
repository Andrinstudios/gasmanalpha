﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class GMTestFeedback : MMFeedback
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    override protected void CustomPlayFeedback(Vector3 position, float attenuation = 1)
    {
        Debug.Log("Playing test feedback");

    }
}
