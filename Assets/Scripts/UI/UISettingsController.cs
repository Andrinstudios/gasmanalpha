using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;

public class UISettingsController : MonoBehaviour
{
    public Slider masterVolumeSlider;
    public Slider sFXVolumeSlider;
    public Slider musicVolumeSlider;

    public virtual void AdjustMasterVolume()
    {
        MMSoundManager.Instance.SetVolumeMaster(masterVolumeSlider.value);
    }

    public virtual void AdjustSfxVolume()
    {
        MMSoundManager.Instance.SetVolumeSfx(sFXVolumeSlider.value);
    }

    public virtual void AdjustMusicVolume()
    {
        MMSoundManager.Instance.SetVolumeMusic(musicVolumeSlider.value);
    }

    public virtual void SetSliderValues()
    {
        masterVolumeSlider.SetValueWithoutNotify(MMSoundManager.Instance.settingsSo.GetTrackVolume(MMSoundManager.MMSoundManagerTracks.Master));
        musicVolumeSlider.SetValueWithoutNotify(MMSoundManager.Instance.settingsSo.GetTrackVolume(MMSoundManager.MMSoundManagerTracks.Music));
        sFXVolumeSlider.SetValueWithoutNotify(MMSoundManager.Instance.settingsSo.GetTrackVolume(MMSoundManager.MMSoundManagerTracks.Sfx));
    }

    public virtual void SaveVolumeSettings()
    {
        MMSoundManager.Instance.SaveSettings();
    }
}
