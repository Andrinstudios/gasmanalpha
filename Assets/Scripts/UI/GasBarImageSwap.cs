using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;

public class GasBarImageSwap : MonoBehaviour, MMEventListener<PickableItemEvent>
{
    public Image iceImage;
    public Image fireImage;

    public MMFeedbacks iceStartFeedback;
    public MMFeedbacks fireStartFeedback;

    private void OnEnable()
    {
        this.MMEventStartListening<PickableItemEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<PickableItemEvent>();
    }

    public void OnMMEvent(PickableItemEvent pEvent)
    {
        PickableItem pickedItem = pEvent.PickedItem;

        if (pickedItem.tag == "Fire")
        {
            SetFireImage();
            return;
        }

        if (pickedItem.tag == "Ice")
        {
            SetIceImage();
            return;
        }
    }

    private void SetFireImage()
    {
        fireImage.enabled = true;
        StartCoroutine(ImageTimer(fireImage));
    }

    private void SetIceImage()
    {
        iceImage.enabled = true;
        StartCoroutine(ImageTimer(iceImage));
    }

    IEnumerator ImageTimer(Image _image)
    {
        yield return new WaitForSeconds(7);

        for (int i = 0; i <= 3; i++)
        {
            Debug.Log("i = " + i);
            _image.enabled = false;
            yield return new WaitForSeconds(0.5f);
            _image.enabled = true;

            yield return new WaitForSeconds(0.5f);
        }

        _image.enabled = false;
        yield return null;
       
    }
}
