﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUITimeable
{
    bool isTimerComplete{get;set;}

    bool BeginTimer();

}
