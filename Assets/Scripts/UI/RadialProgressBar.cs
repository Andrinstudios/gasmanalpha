﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialProgressBar : MonoBehaviour
{
	public GameObject LoadingText;
	public Text ProgressIndicator;
	public Image LoadingBar;
	float currentValue;
	public float speed;
	public float rechargeTime;
	private float timer;
 
	// Use this for initialization
	void Start () {
		timer = rechargeTime;
	}
	
	// Update is called once per frame
	void Update () 
    {
		StartTimer();
		
/*
		if (currentValue < 100) {
			currentValue += speed * Time.deltaTime;
			//ProgressIndicator.text = ((int)currentValue).ToString () + "%";
			//LoadingText.SetActive (true);
		} else {
			//LoadingText.SetActive (false);
			//ProgressIndicator.text = "Done";
	    }
 
		LoadingBar.fillAmount = currentValue / 100;
*/
    }

	public void StartTimer()
	{
		
		if (timer >= 0)
		{
			LoadingBar.fillAmount = timer/rechargeTime;
			timer -= Time.deltaTime;
			Debug.Log("timer = "+ timer);
		}
	}
}

