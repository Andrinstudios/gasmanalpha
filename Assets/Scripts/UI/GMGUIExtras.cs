using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MoreMountains.Tools;

public class GMGUIExtras : MonoBehaviour
{
    public GameObject OptionsScreen;
    public GameObject PauseScreen;

    public GameObject pauseInitalButton;
    public GameObject optionsInitialButton;

    protected bool optionsVisible = false;

    public virtual void ToggleOptionScreen()
    {
        if (OptionsScreen != null)
        {
            optionsVisible = !optionsVisible;
            OptionsScreen.SetActive(optionsVisible);
            PauseScreen.SetActive(!optionsVisible);

            //select the initial button based on the screen that is visible.
            if (optionsVisible)
            {
                EventSystem.current.SetSelectedGameObject(optionsInitialButton);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(pauseInitalButton);
            }
        }
    }
}
