using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIExplode : AIAction
{
    public override void PerformAction()
    {
        ExplodingAttack _explode = GetComponent<ExplodingAttack>();
        _explode.StartExplosionSequence();
    }
}
