﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

/// <summary>
/// This action Flips the character to face the target.
/// </summary>
[AddComponentMenu("Corgi Engine/Character/AI/Actions/AI Action Face Target")]
[RequireComponent(typeof(Character))]


public class AIActionFaceTarget : AIAction
{
    protected Character _character;

    public override void Initialization()
    {
        base.Initialization();
        _character = this.gameObject.GetComponent<Character>();
    }

    public override void PerformAction()
    {
        Flip();
    }

    protected void Flip()
    {
        if (this.transform.position.x < _brain.Target.position.x)
        {
            if(!_character.IsFacingRight)
            {
                _character.Flip();
            }
        }

        if (this.transform.position.x > _brain.Target.position.x)
        {
            if(_character.IsFacingRight)
            {
                _character.Flip();
            }
        }

    }

}
