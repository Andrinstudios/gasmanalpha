using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;




public class AIActionSummon : AIAction
{
    protected GMCharacterSummon _summon;
    protected GMCharacterSummonerBosz _bossSummon;
    private Transform _playerTransform;

    protected void Start()
    {
        //we need to set the AIBrain.Target to the player
      //  _playerTransform = LevelManager.Instance.Players[0].transform;

    }

    public override void Initialization()
    {
   
        _bossSummon = this.gameObject.GetComponent<GMCharacterSummonerBosz>();
        if (_bossSummon == null)
        {
            Debug.Log("Boss is null");
            _summon = GetComponent<GMCharacterSummon>();
        }

       // _playerTransform = LevelManager.Instance.Players[0].transform;
    }

    public override void PerformAction()
    {
        if (_brain.Target == null)
        {
            _brain.Target = GetPlayerTransform();
        }

        if (_summon != null)
        {
            _summon.Summon(_brain.Target);
        }

        if (_bossSummon != null)
        {
            Debug.Log("Calling summon");
            _bossSummon.Summon(_brain.Target);
        }
    }

    protected Transform GetPlayerTransform()
    {
        Transform _pTrans = LevelManager.Instance.Players[0].transform;
        if (_pTrans != null)
        {
            return _pTrans;
        }
        else
        {
            Debug.Log("No Player transform found");
            return null;

        }
    }
}
