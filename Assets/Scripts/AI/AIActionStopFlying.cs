using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class AIActionStopFlying : AIAction
{

    protected CharacterFly _fly;
    protected FlyingDash _dash;


    public override void Initialization()
    {
        _fly = GetComponent<CharacterFly>();
        _dash = GetComponent<FlyingDash>();
    }

    public override void PerformAction()
    {
        if (_fly != null && _dash != null)
        {
            _fly.SetHorizontalMove(0);
            _fly.SetVerticalMove(0);
            _dash.PlayPredashFeedbacks();
        }
        else
        {
            Debug.Log("AIActionStopFlying reqiores CharacterFly and FlyingDash");
        }
        
    }

}
