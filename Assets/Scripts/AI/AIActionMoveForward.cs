﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

/// <summary>
/// This action directs the CharacterHorizontalMovement ability to move in the direction of the target.
/// </summary>
[AddComponentMenu("Corgi Engine/Character/AI/Actions/AI Action Move Forward while jumping")]
[RequireComponent(typeof(CharacterHorizontalMovement))]


public class AIActionMoveForward : AIAction
{

    protected CharacterHorizontalMovement _characterHorizontalMovement;
    protected float direction = 1f;

    /// <summary>
    /// On init we grab our CharacterHorizontalMovement ability
    /// </summary>
    public override void Initialization()
    {
        _characterHorizontalMovement = this.gameObject.GetComponent<CharacterHorizontalMovement>();
    }

    /// <summary>
    /// On PerformAction we move
    /// </summary>
    public override void PerformAction()
    {
        Move();
    }

    protected virtual void Move()
    {
        if (_brain.Target == null)
        {
            _characterHorizontalMovement.SetHorizontalMove(0f);
            return;
        }

        if (direction > 0)
        {
            _characterHorizontalMovement.SetHorizontalMove(1f);
        }
        else
        {
            _characterHorizontalMovement.SetHorizontalMove(-1f);
        }
    }

    /// <summary>
    /// When entering the state we reset our movement.
    /// </summary>
    public override void OnEnterState()
    {
        base.OnEnterState();
        _characterHorizontalMovement.SetHorizontalMove(0f);
        if (this.transform.position.x < _brain.Target.position.x)
        {
            direction = 1f;
        }
        else
        {
            direction = -1f;
        }
    }

    /// <summary>
    /// When exiting the state we reset our movement.
    /// </summary>
    public override void OnExitState()
    {
        base.OnEnterState();
        _characterHorizontalMovement.SetHorizontalMove(0f);
    }
}
