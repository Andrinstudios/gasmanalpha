﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class AIDecision2DDistanceToTarget : AIDecision
{ 
    /// The possible comparison modes
    public enum ComparisonModes { StrictlyLowerThan, LowerThan, Equals, GreatherThan, StrictlyGreaterThan }
    /// the method to use to compare the distance (StrictlyLowerThan, LowerThan, Equals, GreatherThan or StrictlyGreaterThan)
    [Tooltip("the method to use to compare the distance (StrictlyLowerThan, LowerThan, Equals, GreatherThan or StrictlyGreaterThan)")]
    public ComparisonModes ComparisonMode = ComparisonModes.GreatherThan;
    /// the distance to compare with
    [Tooltip("the distance to compare with")]
    public float Distance;
    private Transform _playerTransform;

    protected void Start()
    {


        ////we need to set the AIBrain.Target to the player
        //if (_brain.Target == null)
        //{
        //    _playerTransform = LevelManager.Instance.Players[0].transform;
        //}

    }



    /// <summary>
    /// On Decide we check our distance to the Target
    /// </summary>
    /// <returns></returns>
    public override bool Decide()
    {
        return EvaluateDistance();
    }

    /// <summary>
    /// Returns true if the distance conditions are met
    /// </summary>
    /// <returns></returns>
    protected virtual bool EvaluateDistance()
    {
        if (_brain.Target == null)
        {
            _brain.Target = LevelManager.Instance.Players[0].transform;

            //  return false;
        }

        float distance = Vector2.Distance(this.transform.position, _brain.Target.position);

        if (ComparisonMode == ComparisonModes.StrictlyLowerThan)
        {
            return (distance < Distance);
        }
        if (ComparisonMode == ComparisonModes.LowerThan)
        {
            return (distance <= Distance);
        }
        if (ComparisonMode == ComparisonModes.Equals)
        {
            return (distance == Distance);
        }
        if (ComparisonMode == ComparisonModes.GreatherThan)
        {
            return (distance >= Distance);
        }
        if (ComparisonMode == ComparisonModes.StrictlyGreaterThan)
        {
            return (distance > Distance);
        }
        return false;
    }
}
