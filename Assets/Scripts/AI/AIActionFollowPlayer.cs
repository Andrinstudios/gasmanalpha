﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIActionFollowPlayer : AIActionMoveTowardsTarget
{
    public Vector3 HoleDetectionOffset = new Vector3(0, 0, 0);
    /// the length of the ray cast to detect holes
    public float HoleDetectionRaycastLength = 1f;
    /// If set to true, the agent will try and avoid falling
    public bool AvoidFalling = false;

    // private stuff
    protected CorgiController _controller;
    protected Character _character;
    protected Vector2 _direction;
    protected bool isAtHole = false;

    public override void Initialization()
    {
        // we get the CorgiController2D component
        _controller = GetComponent<CorgiController>();
        _character = GetComponent<Character>();
        _direction = _character.IsFacingRight ? Vector2.right : Vector2.left;
        isAtHole = CheckForHoles();
        base.Initialization();
    }

    protected override void Move()
    {
        if (!CheckForHoles())
        {
           base.Move();
            _direction = _character.IsFacingRight ? Vector2.right : Vector2.left;
        }
        else
        {
            _characterHorizontalMovement.SetHorizontalMove(0);
        }
        
       
    }

    /// <summary>
    /// Checks for holes 
    /// </summary>
    protected virtual bool CheckForHoles()
    {
        // if we're not grounded or if we're not supposed to check for holes, we do nothing and exit
        if (!AvoidFalling || !_controller.State.IsGrounded)
        {
            return false;
        }

        // we send a raycast at the extremity of the character in the direction it's facing, and modified by the offset you can set in the inspector.
        Vector2 raycastOrigin = new Vector2(transform.position.x + _direction.x * (HoleDetectionOffset.x + Mathf.Abs(GetComponent<BoxCollider2D>().bounds.size.x) / 2), transform.position.y + HoleDetectionOffset.y - (transform.localScale.y / 2));
        RaycastHit2D raycast = MMDebug.RayCast(raycastOrigin, -transform.up, HoleDetectionRaycastLength, _controller.PlatformMask | _controller.MovingPlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.gray, true);
        // if the raycast doesn't hit anything
        if (!raycast)
        {
            // we stop moving
            isAtHole = true;
            return true;
            
        }
        isAtHole = false;
        return false;
    }


}
