﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


public class AIActionShootMultiple : AIActionShoot
{
    public int maxShots = 3;
    public int minShots = 1;
    public float timeBetweenShots = 0.2f;
    public bool stopToShoot = false;

    protected int shotsToFire = 0;

    protected CharacterHorizontalMovement _characterHorizontalMovement;

    public override void PerformAction()
    {
        TestFaceTarget();
        TestAimAtTarget();
        StartCoroutine(ShootSequence());
        StartCoroutine(StopMovement());
    }


    public override void OnEnterState()
    {
        shotsToFire = Random.Range(minShots+1, maxShots+1);
        base.OnEnterState();
    }

    IEnumerator ShootSequence()
    {

       for (int i = 0; i < shotsToFire; i++)
        {
            Shoot();
            yield return new WaitForSeconds(timeBetweenShots);
        }

        Debug.Log("done");
    }

    protected override void Shoot()
    {
        TargetHandleWeapon.ShootStart();
    }

    IEnumerator StopMovement()
    {
        _characterHorizontalMovement = this.gameObject.GetComponent<CharacterHorizontalMovement>();
        _characterHorizontalMovement.SetHorizontalMove(0);
        _characterHorizontalMovement.MovementForbidden = true;
        Debug.Log("stopped");
        yield return new WaitForSeconds((_numberOfShoots + 1) * timeBetweenShots);
        Debug.Log("move");
        _characterHorizontalMovement.MovementForbidden = false;

    }


}
