﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class AIActionDamageDash : AIAction
{
    protected CharacterDamageDash _characterDash;

    /// <summary>
    /// On init we grab our CharacterDash component
    /// </summary>
    public override void Initialization()
    {
        _characterDash = this.gameObject.GetComponent<CharacterDamageDash>();
    }

    /// <summary>
    /// On PerformAction we dash
    /// </summary>
    public override void PerformAction()
    {
        Dash();
    }

    /// <summary>
    /// Calls CharacterDash's StartDash method to initiate the dash
    /// </summary>
    protected virtual void Dash()
    {
        _characterDash.StartDash();
    }
}

