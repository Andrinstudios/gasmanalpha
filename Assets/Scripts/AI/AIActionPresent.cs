﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIActionPresent : AIAction
{
    private Animator _animator;
    private Character _character;

    //A reference to the Gun Brain to be sure it does not fire before we are ready
    public AIBrain gunBrain;
    

    public override void Initialization()
    {
        _character = this.GetComponent<Character>();
        _animator = _character.CharacterAnimator;
    }

     public override void PerformAction()
     {
        _animator.SetTrigger("Present");
       // _animator.ResetTrigger("Reset");

        //turn on the gun if there is one
        if (gunBrain != null)
        {
            gunBrain.BrainActive = true;
        }
        

        //set the gun brain target to the player
       // gunBrain.Target = LevelManager.Instance.PlayerPrefabs[0].transform;
 }
}
