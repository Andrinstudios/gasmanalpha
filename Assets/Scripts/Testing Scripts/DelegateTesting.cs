﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class DelegateTesting : MonoBehaviour
{

    MMPoolableObject poolableObject;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("test start");
        poolableObject = gameObject.GetComponent<MMPoolableObject>();
        poolableObject.OnSpawnComplete += DelegateTest;
    }


    public void DelegateTest()
    {
        Debug.Log("test working");
    }

}
