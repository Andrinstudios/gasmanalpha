﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using UnityEditor;
using UnityEngine;

public class VacuumWeapon : MeleeWeapon {
    [Header ("Vacuum Properties")]
    public AnimationCurve vacuumCurve = AnimationCurve.EaseInOut(0.0f, 0.0f,1.0f,1.0f);
    public AnimationCurve flameThrowerCurve = AnimationCurve.EaseInOut(0.0f, 0.0f,1.0f,1.0f);
    public float vacuumForce = 75;
    public float maxGasLevel = 50;
    protected Animator tankAnimator;

    [HideInInspector]
    public float currentGasLevel;

    public float weaponDamage = 10;

    public ParticleSystem flameThrowerParticles;
    public Transform flameThrowerTransform;
    public float damageDelay = 1f;

    [Header("Vacuum Feedbacks")]
    public MMFeedbacks SuckingFeedback;

    public AudioClip activeSound;
    public AudioClip weaponChargedSound;
    public AudioClip fireSound;

    private Character _character;
    [HideInInspector]
    public bool isCharged = false;
    private bool isInWeaponState = false;
    private Transform _cloudKiller;
    private IgniteOnTouch _igniteOnTouch;

    [Header("RayCasting")]
    public float rayLength = 3f;
    public float rayAngle = 15f;
    public int numberOfRays = 3;
    public LayerMask cloudMask;

    private void Start () {
        
        currentGasLevel = 0f;
        _character = Owner;
        ActiveDuration = flameThrowerParticles.main.duration;
        _cloudKiller = this.gameObject.transform.GetChild(0);
        GetTankAnimator();
    }

    private void GetTankAnimator()
    {
        Animator[] _anims;

        _anims = Owner.GetComponentsInChildren<Animator>();

        if (_anims.Length > 0)
        {
            foreach (Animator tAnim in _anims)
            {
                if (tAnim.tag == "Tank")
                {
                    tankAnimator = tAnim;
                    break;
                }
            }
        }
    }

    protected override void Update () {

      //check to see if the vacuum is charged 
        if (isCharged && !isInWeaponState) {
            StartCoroutine(BeginWeaponState ());
            isInWeaponState = true;
        }
        else
        {
            CastRays();
        }
        base.Update ();
    }

    protected void CastRays()
    {
        float tempAngle = 90 - rayAngle;
        Vector2 rayDirection;
        
        if (Owner.IsFacingRight)
        {
            rayDirection = Vector2.right;
        }
        else
        {
            rayDirection = Vector2.left;
            tempAngle = -tempAngle;
        }

        float radians = (tempAngle * Mathf.PI) / 180;

        Vector2 angledRayDirection = new Vector2((float)Mathf.Sin(radians), (float)Mathf.Cos(radians));

        RaycastHit2D _angledRay = MMDebug.RayCast(this.transform.position, angledRayDirection, rayLength, cloudMask, MMColors.Gold, true);
        RaycastHit2D _cloudray = MMDebug.RayCast(this.transform.position, rayDirection, rayLength, cloudMask, MMColors.Gold, true);

        if (_cloudray || _angledRay)
        {
            //Check to make sure the cloud is not already moving into vac before moving again.fghj
            CloudProjectile _cloud = _cloudray.collider.gameObject.GetComponent<CloudProjectile>();
            if (_cloud == null)
            {
                _cloud = _cloudray.collider.gameObject.GetComponentInParent<CloudProjectile>();
            }

            if (_cloud.inVacuum)
            {
                return;
            }

            SuckingFeedback.PlayFeedbacks();
            //set flag to inVacuum and turn off damage
            _cloud.inVacuum = true;
            DamageOnTouch cloudDamage = _cloud.GetComponentInChildren<DamageOnTouch>();
            cloudDamage.enabled = false;


            Animator anim = _cloud.GetComponent<Animator>();
            anim.SetBool("InVacuum", true);

            //Move via lerp instead for more realistic effect        
            StartCoroutine(MoveCloudIntoVacuum(_cloud, _cloud.transform.position, 0f));

            AddGas(cloudDamage.MaxDamageCaused);
            
        }
    }

    protected override void CreateDamageArea()
    {
        base.CreateDamageArea();
        _igniteOnTouch = _damageArea.AddComponent<IgniteOnTouch>();
        _igniteOnTouch._layerMask = TargetLayerMask;
        _igniteOnTouch._layerMask |= (1 << LayerMask.NameToLayer("PlayerProjectiles"));
       
    }

    IEnumerator MoveCloudIntoVacuum(CloudProjectile _mCloud, Vector3 start, float time)
    {
        float i = 0.0f;
        while (i < 1)
        {
            time += Time.deltaTime;
            i = time/vacuumForce;
            _mCloud.transform.position = Vector3.Lerp(start,_cloudKiller.position,vacuumCurve.Evaluate(i));

            yield return null;
         }
    }

    IEnumerator RotateFlameThrower()
    {
       // Vector3 from = new Vector3(flameThrowerTransform.position);
       
        yield return null;
        float i = 0.0f;
        float timer = flameThrowerParticles.main.duration;
        while (i < 1)
        {
            timer += Time.deltaTime;
            i = timer/3f;
             float t = (Mathf.Sin (Time.time * 1 * Mathf.PI * 2.0f) + 1.0f) / 2.0f;
            

        }
    }

    public void AddGas (float amount) {
        currentGasLevel += amount;
        if (currentGasLevel >= maxGasLevel) {
            currentGasLevel = maxGasLevel;
            isCharged = true;
        }
        UpdateTankMeter ();
    }

    void RemoveGas (float amount) {
        currentGasLevel -= amount;
        isCharged = false;
        isInWeaponState = false;
        if (currentGasLevel <= 0) {
            currentGasLevel = 0f;
        }

        UpdateTankMeter ();
    }

    IEnumerator BeginWeaponState () {

        yield return new WaitForSeconds(DelayBeforeUse);

        WeaponUse();
    }

    protected override void WeaponUse () {
    
       // Debug.Log ("Start Time " + Time.time);
      // _damageOnTouch.enabled = true;

      //  _character._animator.SetBool("isSucking",false);

        PlayFlames ();
        base.WeaponUse();
        StartCoroutine(FlameThrowerTimer());
  
    }

    IEnumerator FlameThrowerTimer()
    {
        //halt the movement while the flamethrower is being used
        CharacterHorizontalMovement _walk = GetComponentInParent<CharacterHorizontalMovement> ();
        float speed = _walk.MovementSpeed;
        _walk.MovementSpeed = 0;

        EnableDamageArea();
        yield return new WaitForSeconds(ActiveDuration);
        isInWeaponState = false;
        isCharged = false;
        _character._animator.SetBool("isSucking",true);
        RemoveGas(currentGasLevel);
        _walk.MovementSpeed = speed;
        DisableDamageArea();
    }

    IEnumerator FiringSequence () {
        CharacterHorizontalMovement _walk = GetComponentInParent<CharacterHorizontalMovement> ();
        float speed = _walk.MovementSpeed;
        _walk.MovementSpeed = 0;
        yield return new WaitForSeconds (1);

        base.WeaponUse ();
        RemoveGas (maxGasLevel);

        yield return new WaitForSeconds (4);

        _walk.MovementSpeed = speed;
        isInWeaponState = false;
        AIShootOnSite_GM _aiShoot = GetComponentInParent<AIShootOnSite_GM> ();
        _aiShoot.enabled = false;
    }

    IEnumerator PauseWhileFiring (CharacterHorizontalMovement _walk) {
        float speed = _walk.MovementSpeed;
        _walk.MovementSpeed = 0;
        yield return new WaitForSeconds (ActiveDuration);
        _walk.MovementSpeed = speed;
    }

    void UpdateTankMeter () {
        //get the percentage full to update the tank animator
        float tankPercent = currentGasLevel / maxGasLevel;
        tankAnimator.SetFloat("GasLevel", tankPercent);
    }

    void PlayFlames () 
    {
        if (flameThrowerParticles == null)
        {
            Debug.Log("no particles");
            return;
        }
        flameThrowerParticles.Play ();

    }

}