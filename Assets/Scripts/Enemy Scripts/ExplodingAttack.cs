using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class ExplodingAttack : MonoBehaviour
{
    public MMFeedbacks explosionSequence;

    protected Collider2D _collider;
    protected DamageOnTouch _damageOnTouch;
    protected Health _health;
    protected bool exploded;

    protected virtual void Start()
    {
        _collider = GetComponent<Collider2D>();
        _damageOnTouch = GetComponent<DamageOnTouch>();
        _health = GetComponent<Health>();
    }

    public virtual void StartExplosionSequence()
    {
        Debug.Log("explode started");
        if(!exploded)
        {
            explosionSequence?.PlayFeedbacks();
            exploded = true;
        }
    }

    public virtual void Explode()
    {
        _damageOnTouch.enabled = true;
        _collider.enabled = true;
        StartCoroutine(TurnOffCollider());
        Debug.Log("exploded");
    }

    protected IEnumerator TurnOffCollider()
    {
        yield return new WaitForSeconds(0.2f);
        _collider.enabled = false;
    }

    

}
