﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using System;

[RequireComponent(typeof(MMSimpleObjectPooler))]

public class Onion : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    

    public AcidBombProjectile projectile;
    public float timeBetweenShots = 3f;
    public bool followPlayer = false;
    public LayerMask playerMask;
    public float playerRayLength = 10f;
    public float rayVerticalOffset = 0f;

    // dtermines how the object is oriented
    public bool isUpsideDown = false;

    protected Animator _animator;
    [SerializeField]
    protected bool isVisible = false;
    protected bool shouldShootRight = true;
    [SerializeField]
    protected bool isShooting = false;



    private MMSimpleObjectPooler ObjectPooler;

    public Transform projectileSpawnLocation;


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        ObjectPooler = GetComponent<MMSimpleObjectPooler>();
    }

    private void Update()
    {
        if (isVisible && FoundPlayer() && !isShooting)
        {
            //set the layered animation to rotate the correct direction.

            if (isUpsideDown)
            {
                shouldShootRight = !shouldShootRight;
            }

            if (shouldShootRight)
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("ShootRight"), 1f);
                _animator.SetLayerWeight(_animator.GetLayerIndex("ShootLeft"), 0f);
            }
            else
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("ShootLeft"), 1f);
                _animator.SetLayerWeight(_animator.GetLayerIndex("ShootRight"), 0f);
            }

            _animator.SetBool("prepShot", true);
            StartCoroutine(StartShooting());
            isShooting = true;
        }
    }


    //stop the animations from running if this is off screen.
    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        isShooting = false;
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    IEnumerator StartShooting()
    {
        //the time of this delay needs to match the animation "Shooting" on the base object.
        yield return new WaitForSeconds(1.75f);
        Shoot();
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    //Sets the time it takes between shoot and restarting the shooting animation
    IEnumerator ShotTimer()
    {
        yield return new WaitForSeconds(timeBetweenShots);
        isShooting = false;
    }


    //this is called from the animation event on the shooting animation.
    public void Shoot()
    {
        // _charHandleWeapon.ShootStart();
        SpawnProjectile(projectileSpawnLocation.position, 0, 1, true);
        _animator.SetBool("prepShot", false);

        //start the shot timer
        StartCoroutine(ShotTimer());
        
    }

    //looks for player to trigger the shooting and dtermine the direction of the shot.
    private bool FoundPlayer()
    {
        Vector3 offset = new Vector3(this.transform.position.x, this.transform.position.y + rayVerticalOffset, this.transform.position.z);


        RaycastHit2D _playerRayRight = MMDebug.RayCast(offset, Vector2.right, playerRayLength, playerMask, Color.red, true);
        if (_playerRayRight)
        {
            shouldShootRight = true;

            return true;

        }


        RaycastHit2D _playerRayLeft = MMDebug.RayCast(offset, Vector2.left, playerRayLength, playerMask, Color.red, true);
        if (_playerRayLeft)
        {
            shouldShootRight = false;
          
            return true;
        }

        return false;

    }

    /// <summary>
    /// Spawns a new object and positions/resizes it
    /// </summary>
    public virtual GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles, bool triggerObjectActivation = true)
    {
        /// we get the next object in the pool and make sure it's not null
        GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

        // mandatory checks
        if (nextGameObject == null) { return null; }
        if (nextGameObject.GetComponent<MMPoolableObject>() == null)
        {
            throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
        }
        // we position the object
        nextGameObject.transform.position = spawnPosition;
        // we set its direction

        Projectile projectile = nextGameObject.GetComponent<Projectile>();
        if (projectile != null)
        {
            projectile.SetOwner(this.gameObject);
        }
        // we activate the object
        nextGameObject.gameObject.SetActive(true);


        if (projectile != null)
        {
            /*
            if (RandomSpread)
            {
                _randomSpreadDirection.x = UnityEngine.Random.Range(-Spread.x, Spread.x);
                _randomSpreadDirection.y = UnityEngine.Random.Range(-Spread.y, Spread.y);
                _randomSpreadDirection.z = UnityEngine.Random.Range(-Spread.z, Spread.z);
            }
            else
            {
                if (totalProjectiles > 1)
                {
                    _randomSpreadDirection.x = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.x, Spread.x);
                    _randomSpreadDirection.y = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.y, Spread.y);
                    _randomSpreadDirection.z = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.z, Spread.z);
                }
                else
                {
                    _randomSpreadDirection = Vector3.zero;
                }
            }

            Quaternion spread = Quaternion.Euler(_randomSpreadDirection);
            
            projectile.SetDirection(spread * transform.right * (Flipped ? -1 : 1), transform.rotation, Owner.IsFacingRight);
            if (RotateWeaponOnSpread)
            {
                this.transform.rotation = this.transform.rotation * spread;
            }
            */
        }

        Vector3 _direction;

        if (shouldShootRight)
        {
            _direction = new Vector3(1, 1, 0);
            if(isUpsideDown)
            {
                _direction = new Vector3(1, -0.5f, 0);
            }
        }
        else
        {
            _direction = new Vector3(-1, 1, 0);
            if (isUpsideDown)
            {
                _direction = new Vector3(-1, -0.5f, 0);
            }
        }


        projectile.SetDirection(_direction, transform.rotation, true);


        if (triggerObjectActivation)
        {
            if (nextGameObject.GetComponent<MMPoolableObject>() != null)
            {
                nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
            }
        }

        return (nextGameObject);
    }

    //assure the object is returned to starting position.
    protected void ResetAnimator()
    {
        _animator.SetLayerWeight(_animator.GetLayerIndex("ShootRight"), 0f);
        _animator.SetLayerWeight(_animator.GetLayerIndex("ShootLeft"), 0f);
    }


    //Catch the respawn event in order to reset the amimator.
    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                //  Debug.Log("receieved event on " + gameObject.name);
                if (!GetComponent<ProximityManaged>().DisabledByManager)
                {
                    ResetAnimator();
                }

                break;

            default:
                break;
        }
    }
}
