﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class BugProtector : MonoBehaviour
{
    public float duration;
    public LayerMask _collisionMask;
    public bool isProtecting = false;
    private BoxCollider2D _collider;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _collider.enabled = false;
        isProtecting = false;
    }

    private void Update()
    {
        
    }

    public void TurnOnCollider()
    {
        _collider.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check that object is in the mask
        int layer = collision.gameObject.layer;
        if (_collisionMask != (_collisionMask | (1 << layer)))
        {
            return;
        }

        if (!isProtecting)
        {
            return;
        }

        //this is very inelegant - should replace with an animation/fade/sound...
        CloudProjectile _cloud = collision.gameObject.GetComponentInParent < CloudProjectile >();
        if (!_cloud)
        {
            return;
        }

        BoxCollider2D _cloudCollider = _cloud.GetComponent<BoxCollider2D>();
        _cloudCollider.enabled = false;

        DamageOnTouch _cloudDamager = _cloud.gameObject.GetComponentInChildren<DamageOnTouch>();
      //  Debug.Log("cloud collided " + _cloud.name);
        _cloudDamager.enabled = false;
        StartCoroutine(RemoveCloud(_cloud));
        // _cloud.Destroy();
    }

    IEnumerator RemoveCloud(CloudProjectile _thisCloud)
    {
        yield return new WaitForSeconds(duration);
        BoxCollider2D _col = _thisCloud.GetComponent<BoxCollider2D>();
        _col.enabled = true;
        DamageOnTouch _cloudDamager = _thisCloud.gameObject.GetComponentInChildren<DamageOnTouch>();
        _cloudDamager.enabled = true;
        _thisCloud.Destroy();
    }

    public IEnumerator ProtectBugs()
    {
       // Debug.Log("protecting");
        isProtecting = true;
        yield return new WaitForSeconds(duration);
        isProtecting = false;
      //  Debug.Log("not protecting");
    }
}
