﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;


public class EnemyHealth : Health
{

    //TODO determine workflow for attack damage
    /* 1. Should this script hold the responses to each damage type?
     * 2. What if data was stored in seperate script?
     * 3. what data needs to be stored
     *      Animations per type
     *      Sound FX per type
     *      Particles per type
     *      Force applied per type
     * 4. Can these be stored in a dictionary and pulled up ar runtime
     *      would this be serializable in the inspector
    */
    [Header("Special Attack Dictionaries")]
    public Dictionary<SpecialAttack.Attack, AudioClip[]> damageSFX_Dict;
    public Dictionary<SpecialAttack.Attack, ParticleSystem[]> damagePartitle_Dict;

    public AudioClip[] atomicDamageSFX;
    public AudioClip[] shartDamageSFX;
    public AudioClip[] damageSFX;



    public virtual void ApplySpecialDamage(int damageAmount, SpecialAttack.Attack attackType)
    {
        Debug.Log("special damage");
        switch (attackType)
        {
            case SpecialAttack.Attack.Atomic:
                {
                  //  AudioClip[] attackClips = damageSFX_Dict[SpecialAttack.Attack.Atomic];
                  if (atomicDamageSFX.Length > 0)
                  {
                    AudioClip thisClip = atomicDamageSFX[Random.Range(0, atomicDamageSFX.Length)];
                    PlaySpecialAttackClip(thisClip);
                  }
                    StartCoroutine(DamageAfterDelay(MaximumHealth, 0.5f));
                    break;
                }

            case SpecialAttack.Attack.SBD:
                {
                    break;
                }

            case SpecialAttack.Attack.Shart:
                {
                    break;
                }
            default:
                break;
        }
    }

    void PlaySpecialAttackClip(AudioClip _clip)
    {
        SoundManager.Instance.PlaySound(_clip, transform.position, false);
    }




    IEnumerator DamageAfterDelay(float damage, float delayTime)
    {
        Debug.Log("damage Dealy start"+ Time.time);
        yield return new WaitForSeconds(delayTime);
        Damage(damage, null, 1f, 1f, new Vector3(0,0,0));
        Debug.Log("damage applied" + Time.time);

    }
}
