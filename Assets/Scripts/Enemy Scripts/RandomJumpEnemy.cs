﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class RandomJumpEnemy : MonoBehaviour
{

    private CharacterJump _charJump;
    [MMReadOnly]
    public bool isJumping = false;

    // Start is called before the first frame update
    void Start()
    {
        _charJump = this.GetComponent<CharacterJump>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isJumping)
        {
            StartCoroutine(JumpTimer());
            isJumping = true;
        }
        
    }

    IEnumerator JumpTimer()
    {
        float delay = Random.Range(0.5f, 2f);
        yield return new WaitForSeconds(delay);
        _charJump.JumpStart();
        
        isJumping = false;

    }

    private void OnDisable()
    {
        isJumping = false;
    }
}
