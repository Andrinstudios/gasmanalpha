using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using System;

public class ShockwaveAttack : CharacterAbility
{
    [MMInformation("Attach this component to a game object along with an ObjectPooler to hold the actual shockwave Projectile object. Trigger it from AI or script as you see fit ",MMInformationAttribute.InformationType.Info,false)]

    public bool shootBothDirections = false;

    private MMSimpleObjectPooler ObjectPooler;

    public Transform projectileSpawnLocation;
    public float horizontalOffset = 1f;

    private bool Flipped;

    protected override void Start()
    {
        ObjectPooler = GetComponent<MMSimpleObjectPooler>();
        
        base.Start();
    }


    public override void ProcessAbility()
    {
  
        base.ProcessAbility();
    } 

    public void Shoot()
    {
        Flipped = !_character.IsFacingRight;
        SpawnProjectile(this.transform.position, 0, 1, true);

        if (shootBothDirections)
        {
            Flipped = !Flipped;
            SpawnProjectile(projectileSpawnLocation.position, 0, 1, true);
        }
        
    }


    public virtual GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles, bool triggerObjectActivation = true)
    {
        /// we get the next object in the pool and make sure it's not null
        GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

        // mandatory checks
        if (nextGameObject == null) { return null; }
        if (nextGameObject.GetComponent<MMPoolableObject>() == null)
        {
            throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
        }

        //determine the type of collider on the oject in order to set the height appropriately.

        Collider2D col = nextGameObject.GetComponent<Collider2D>();

        if (col is CircleCollider2D)
        {
            nextGameObject.transform.position = GetSpawnLocation(nextGameObject.GetComponent<CircleCollider2D>().radius);
        }

        if (col is BoxCollider2D)
        {
            nextGameObject.transform.position = GetSpawnLocation(nextGameObject.GetComponent<BoxCollider2D>().size.y/2);
        }

        if (col is PolygonCollider2D)
        {
            nextGameObject.transform.position = GetSpawnLocation(nextGameObject.GetComponentInChildren<SpriteRenderer>().bounds.size.y / 2);
            Debug.Log("collider " + nextGameObject.GetComponentInChildren<SpriteRenderer>().bounds.size.y / 2);
        }

        // we set its direction

        Projectile projectile = nextGameObject.GetComponent<Projectile>();
        if (projectile != null)
        {
            projectile.SetOwner(this.gameObject);
        }
        // we activate the object
        nextGameObject.gameObject.SetActive(true);


        projectile.SetDirection(transform.right * (Flipped ? -1 : 1), transform.rotation, _character.IsFacingRight);

        
        if (triggerObjectActivation)
        {
            if (nextGameObject.GetComponent<MMPoolableObject>() != null)
            {
                nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
            }
        }

        return (nextGameObject);
    }


   protected virtual Vector3 GetSpawnLocation(float colliderHeight)
    {
        Vector3 spawnLocation = new Vector3(0,0,0);

        /* Cast a ray downward from the spawn object and find where is intersects the platform
         * Move the object so the bottom aligns with the top of the platform and return it
        */



        RaycastHit2D _platformRay = MMDebug.RayCast(projectileSpawnLocation.position, Vector2.down, 1f, _controller.PlatformMask, Color.red, true);

        if (_platformRay)
        {
            Debug.Log("point " + _platformRay.point);

            if(!shootBothDirections)
            {
                if (_character.IsFacingRight)
                {
                    Vector2 newLoc = new Vector2(_platformRay.point.x + horizontalOffset, _platformRay.point.y + colliderHeight);
                    Debug.Log("new point " + newLoc);
                    return newLoc;
                }
                else
                {
                    Vector2 newLoc = new Vector2(_platformRay.point.x - horizontalOffset, _platformRay.point.y + colliderHeight);
                    Debug.Log("new point " + newLoc);
                    return newLoc;
                }
            }
            else
            {
                if (_character.IsFacingRight)
                {
                    Vector2 newLoc = new Vector2(_platformRay.point.x + horizontalOffset, _platformRay.point.y + colliderHeight);
                    Debug.Log("new point " + newLoc);
                    return newLoc;
                }
                else
                {
                    Vector2 newLoc = new Vector2(_platformRay.point.x - horizontalOffset, _platformRay.point.y + colliderHeight);
                    Debug.Log("new point " + newLoc);
                    return newLoc;
                }
            }



            
        }

        return spawnLocation;
    }
   
}
