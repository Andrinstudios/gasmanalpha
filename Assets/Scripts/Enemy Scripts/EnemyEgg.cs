﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


public class EnemyEgg : MonoBehaviour
{

    public MMObjectPooler _enemyPooler { get; set; }

    public GameObject enemyLeader;
    public GameObject enemyFollower;
    public int numberOfEnemies;
    public LayerMask _collisionMask;

    public float hatchDelay = 1;
    public MMFeedbacks hatchFeedbacks;
  //  bool isHatched = false;

    protected Health _health;

    private void Start()
    {
        enemyLeader.SetActive(false);
        Initialization();
    }

    protected virtual void Initialization()
    {
        if (GetComponent<MMMultipleObjectPooler>() != null)
        {
            _enemyPooler = GetComponent<MMMultipleObjectPooler>();
        }
        if (GetComponent<MMSimpleObjectPooler>() != null)
        {
            _enemyPooler = GetComponent<MMSimpleObjectPooler>();
        }
        if (_enemyPooler == null)
        {
            Debug.LogWarning(this.name + " : no object pooler (simple or multiple) is attached to this Projectile Weapon, it won't be able to shoot anything.");
            return;
        }

        _health = GetComponent<Health>();
        
    }
/*
    private void OnTriggerEnter2D(Collider2D other)
    {
        //check that object is in the mask
        int layer = other.gameObject.layer;
        if (_collisionMask != (_collisionMask | (1 << layer)))
        {
            return;
        }

        if (isHatched)
        {
            return;
        }

        //break open the egg
        isHatched = true;
        Hatch();
    }
*/


    public void Hatch()
    {
        /* steps to complete when egg hathes
         * 1.  animate the egg breaking
         * 2. prepare the enemies
         * 3. animate thier appearance
         * 4. remove the egg
         */

        //Trigger the egg animation

        //Trigger Hatching Feedbacks
      //  hatchFeedbacks?.UnityEngine.PlayerLoop.Initialization(gameObject);
        hatchFeedbacks?.PlayFeedbacks();

        //Spawn the leader object
        StartCoroutine(SpawnEnemyLeader());
        ParticleSystem _eggParticles = GetComponentInChildren<ParticleSystem>();
        _eggParticles.Play();
        StartCoroutine(DestroyEgg());

    }


    private void CreateBugs(GameObject leader)
    {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            GameObject nextGameObject = _enemyPooler.GetPooledGameObject();

            // mandatory checks
            if (nextGameObject == null) { break; }
            if (nextGameObject.GetComponent<MMPoolableObject>() == null)
            {
                throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
            }
            // we position the object
            nextGameObject.transform.position = this.transform.position;
            nextGameObject.name = this.gameObject.name + " follower_" + i;
           
         //   Debug.Log("creating " + nextGameObject.name);

            // we activate the object
            nextGameObject.gameObject.SetActive(true);
          //  nextGameObject.GetComponent<BugHealth>().StarttInvulnerableTImer();
            nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();


            //set the inital movement
            CorgiController _enemyController = nextGameObject.GetComponent<CorgiController>();
            float x = UnityEngine.Random.Range(-5, 5);
            float y = UnityEngine.Random.Range(5, 10);
            _enemyController.SetForce(new Vector2(x, y));

            // Assign the bug to the leader
            BugHealth _enemyHealth = _enemyController.gameObject.GetComponent<BugHealth>();
            _enemyHealth.AssignLeader(leader);
            _enemyHealth.Revive();

            AIBugFollow _follow = _enemyController.gameObject.GetComponent<AIBugFollow>();
            _follow.SetLeader(leader);

            //add the follower to the respawn dictionary and set the flags to not respwan
            EnemyData eData = new EnemyData();
            eData.health = _enemyHealth.InitialHealth;
            eData.position = this.transform.position;
            eData.wasInstantiated = true;

            SceneDataHolder _sceneData = FindObjectOfType<SceneDataHolder>();
            _sceneData.AddToRespawnDictionary(nextGameObject.name, eData);

        }


        return;
        //prefab instantiation approach
        if (enemyFollower == null)
        {
            Debug.Log("no follower assigned");
            return;
        }
        for (int i = 0; i < numberOfEnemies; i++)
        {
            GameObject newEnemyFollower = Instantiate(enemyFollower, gameObject.transform.position, Quaternion.identity) as GameObject;
            newEnemyFollower.name = gameObject.name + "_follower_" + i;
            newEnemyFollower.gameObject.transform.position = this.gameObject.transform.position;
            newEnemyFollower.SetActive(true);
            

            //set the inital movement
            CorgiController _enemyController = newEnemyFollower.GetComponent<CorgiController>();
            float x = UnityEngine.Random.Range(-5, 5);
            float y = UnityEngine.Random.Range(5, 10);
            _enemyController.SetForce(new Vector2(x, y));

            // Assign the bug to the leader
            BugHealth _enemyHealth = _enemyController.gameObject.GetComponent<BugHealth>();
            _enemyHealth.AssignLeader(leader);

            AIBugFollow _follow = _enemyController.gameObject.GetComponent<AIBugFollow>();
            _follow.SetLeader(leader);

            //add the follower to the respawn dictionary and set the flags to not respwan
            EnemyData eData = new EnemyData();
            eData.health = 0;
            eData.position = this.transform.position;
            eData.wasInstantiated = true;

            SceneDataHolder _sceneData = FindObjectOfType<SceneDataHolder>();
            _sceneData.AddToRespawnDictionary(newEnemyFollower.name, eData);

        } 

    }

    IEnumerator DestroyEgg()
    {
        yield return new WaitForSeconds(hatchDelay);

        this.gameObject.SetActive(false);
    }

    //Spawn the EnemyLeader along with the gas cloud to protect the bugs
    IEnumerator SpawnEnemyLeader()
    {
        yield return new WaitForSeconds(hatchDelay);

        //activate the leader object
        //enemyLeader.SetActive(true);
        //LeaderCounter _lCounter = enemyLeader.GetComponent<LeaderCounter>();
        //_lCounter.numberOfEnemies = numberOfEnemies;

        //instantiate the leader
        GameObject leader = Instantiate(enemyLeader, transform.position, Quaternion.identity);
        leader.name = gameObject.name + "_" + leader.name;
        leader.SetActive(true);
        LeaderCounter _lCounter = leader.GetComponent<LeaderCounter>();
        _lCounter.numberOfEnemies = numberOfEnemies;


        //release the protective gas...
        BugProtector _protector = GetComponentInChildren<BugProtector>();
        _protector.TurnOnCollider();

        //brief pause before bugs are started
       // yield return new WaitForSeconds(1f);

        //Cleared for bug creation
  
        CreateBugs(leader);

    }

}
