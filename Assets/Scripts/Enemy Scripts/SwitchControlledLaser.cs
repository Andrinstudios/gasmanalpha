﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchControlledLaser : Laser
{
    private void Start()
    {
        canFire = false;
    }

    public override void TurnOnLaser()
    {
        canFire = true;
        
    }

    public override void TurnOffLaser()
    {
        canFire = false;
        isFiring = false;
        StopLaser();
    }

    protected override void Update()
    {
        if(canFire && !isFiring)
        {
            startFeedbacks?.PlayFeedbacks(laserAttachment.transform.position, 1f);
            isFiring = true;
            laserLineRenderer.enabled = true;
        }

        if(canFire)
        {
            StartLaser();
        }
    }

    //protected override void StartLaser()
    //{
    //    CastLaserRay();
    //    DrawLaser();
    //    originFeedbacks?.PlayFeedbacks(laserAttachment.transform.position, 1f);
    //}

    //protected override void StopLaser()
    //{
    //    startFeedbacks?.StopFeedbacks();
    //    originFeedbacks?.StopFeedbacks();
        
    //    laserLineRenderer.enabled = false;
    //    contactFeedbacks?.StopFeedbacks();
    //}

}
