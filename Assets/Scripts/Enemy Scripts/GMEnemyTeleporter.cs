using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMEnemyTeleporter : CharacterAbility, MMEventListener<CorgiEngineEvent>
{
    [MMInformation("this is a list of tgransforms where the enemyt can relocate to.  Add a new Parnet game object to the scene and then add the lcoations as children of it.  do not attach it to the enemy.", MoreMountains.Tools.MMInformationAttribute.InformationType.Info, false)]
    public List<Transform> locations;
    public float delayBeforeTeleport = 2;
    public float delayBeforeReapper = 4;

    public MMFeedbacks disappearFeedback;
    public MMFeedbacks reappearFeedback;

    protected Renderer _renderer;
    protected DamageOnTouch _damageOnTouch;
    protected int locationIndex = 0;

    public delegate void OnTeleportDelegate();
    public OnTeleportDelegate OnTeleport;

    protected override void Initialization()
    {
        base.Initialization();
        _renderer = GetComponent<SpriteRenderer>();
        if (_renderer == null)
        {
            _renderer = GetComponentInChildren<SpriteRenderer>();
        }

        if (_renderer == null)
        {
            Debug.Log("unable to get renderer for summoner");
        }

        _damageOnTouch = GetComponent<DamageOnTouch>();

    }

    public virtual void Teleport()
    {
        /* 1.  Start the sequence by triggering animation
         * 2.  Fade out
         * 3.  Select random destination from list
         * 4.  move character
         * 5.  Fade in
         * 6.  Return to action
         * */

        if(_animator != null)
        {
            _animator.SetTrigger("Teleport");
        }
        
        OnTeleport?.Invoke();

        StartCoroutine(StartTeleport());

    }

    protected virtual IEnumerator StartTeleport()
    {
        yield return new WaitForEndOfFrame();
        disappearFeedback?.PlayFeedbacks();
        yield return new WaitForSeconds(delayBeforeTeleport - 0.5f);
        Disappear();
        Move();

        yield return new WaitForSeconds(delayBeforeReapper / 2);
        reappearFeedback?.PlayFeedbacks();
        yield return new WaitForSeconds(delayBeforeReapper / 2);
        Reappear();
    }

    protected virtual void Disappear()
    {
        Health _health = GetComponent<Health>();
        _health.DamageDisabled();
        _renderer.enabled = false;
        _damageOnTouch.enabled = false;

    }

    protected virtual void Move()
    {
        //select destination
        int newIndex = Random.Range(0, locations.Count);
        while (newIndex == locationIndex)
        {
            newIndex = Random.Range(0, locations.Count);
        }
        Transform _newLocation = locations[newIndex];

        this.gameObject.transform.position = _newLocation.position;
        locationIndex = newIndex;

    }

    protected virtual void Reappear()
    {
        _renderer.enabled = true;
        Health _health = GetComponent<Health>();
        _health.DamageEnabled();
        _damageOnTouch.enabled = true;
    }

    protected virtual void Reset()
    {
        _renderer.enabled = true;
        Health _health = GetComponent<Health>();
        _health.DamageEnabled();
        _damageOnTouch.enabled = true;
        locationIndex = 0;
    }

    protected override void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
        base.OnEnable();
    }

    protected override void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
        base.OnDisable();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                if (_renderer != null)
                {
                    Reset();
                }
                

                break;
            default:
                break;
        }
    }
}
