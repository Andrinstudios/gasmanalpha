using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class ShellEnemy : MonoBehaviour
{

    /*
     * this enemy should deploy its shell when the player comes near
     * - not take any damage
     * - repell all gas clouds
     * - still give damage
     * - not allow the payer to pass through
     * Open back up when player out of range and gas not colliding
     * 
     * Can only take damage from ranged projetiles
     */


    public float shellTimer = 3;
    public ParticleSystem _exhaustParticles;
    public MMFeedbacks _exhaustFeedbacks;
    public float rayLength = 5f;
    public LayerMask playerMask;

    protected Animator _animator;
    protected CapsuleCollider2D _collider;
    protected Health _heatlh;


    [MMReadOnly]
    public List<GameObject> _clouds = new List<GameObject>();
    public SpriteRenderer _renderer;

    protected bool isClosed = false;

    private void Awake()
    {
        _animator = this.gameObject.GetComponent<Animator>();
        _collider = this.gameObject.GetComponent<CapsuleCollider2D>();
        _heatlh = this.gameObject.GetComponentInParent<Health>();
        
    }

    private void Update()
    {
        //if (isClosed && _clouds.Count == 0 && !CastRays())
        //{
        //    RetractShell();
        //}

        //if (!isClosed && CastRays())
        //{
        //    DeployShell();
        //}

        if (CheckForColliders())
        {
            DeployShell();
        }
        else
        {
            RetractShell();
        }

    }


    protected bool CheckForColliders()
    {
        //get the array of all the colliders within the capsuleCollider2D
        Collider2D[] results = Physics2D.OverlapCapsuleAll(transform.position, _collider.size, _collider.direction, 0, playerMask);

        foreach (Collider2D col in results)
        {
            //we only want to react to the player and cloud projecdtiles so other player projecdtiles can get throught to do damage
            if (col.gameObject.CompareTag("Player") || col.gameObject.CompareTag("Cloud"))
            {
                return true;
            }
        }

        return false;
    }



    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Cloud"))
    //    {
    //        _clouds.Add(collision.gameObject);
    //        if(!isClosed)
    //        {
    //           // DeployShell();
    //        }
    //    }
    //}

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Cloud"))
    //    {
    //        Debug.Log("Adding cloud");
    //        _clouds.Remove(collision.gameObject);
    //    }

    //    if (_clouds.Count == 0)
    //    {
    //       // StartCoroutine(ShellTimer());
    //    }
    //}

    public virtual void DeployShell()
    {
        isClosed = true;
      //  _collider.enabled = true;
        _heatlh.Invulnerable = true;

        _exhaustFeedbacks?.PlayFeedbacks();
        _exhaustParticles.Play();
        _renderer.color = Color.green;
      //  StartCoroutine(ShellTimer());
    }

    IEnumerator ShellTimer()
    {
        yield return new WaitForSeconds(shellTimer);
        RetractShell();
    }

    protected virtual void RetractShell()
    {
        _heatlh.Invulnerable = false;
      //  _collider.enabled = false;
        _exhaustFeedbacks?.StopFeedbacks();
        _exhaustParticles.Stop();
        _renderer.color = Color.white;
        isClosed = false;

        _clouds = new List<GameObject>();
    }

    //protected virtual bool CastRays()
    //{
    //    RaycastHit2D playerRayLeft = MMDebug.RayCast(transform.position, Vector3.left, rayLength, playerMask, Color.red, true);

    //    if (playerRayLeft)
    //    {
    //        return true;
    //    }

    //    RaycastHit2D playerRayRight = MMDebug.RayCast(transform.position, Vector3.right, rayLength, playerMask, Color.red, true);

    //    if (playerRayRight)
    //    {
    //        return true;
    //    }

    //    Vector3 upLeft = new Vector3(-1, 1, 0);

    //    RaycastHit2D playerRayUpperLeft = MMDebug.RayCast(transform.position, upLeft, rayLength, playerMask, Color.red, true);

    //    if (playerRayUpperLeft)
    //    {
    //        return true;
    //    }

    //    Vector3 upRight = new Vector3(1, 1, 0);

    //    RaycastHit2D playerRayUpperRight = MMDebug.RayCast(transform.position, upRight, rayLength, playerMask, Color.red, true);

    //    if (playerRayUpperRight)
    //    {
    //        return true;
    //    }

    //    return false;
    //}



    protected virtual void Reset()
    {

    }
}
