﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;


public class FairyReset : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    // Start is called before the first frame update
    void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    void OnDisable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }


    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                ResetFairy();
                break;
            default:
                break;

        }
    }

    void ResetFairy()
    {
        // reset the animator to the inital state

        GetComponent<Animator>().ResetTrigger("Present");
        GetComponent<Animator>().SetTrigger("Reset");

        // reset the Brain to the inital state

        GetComponentInParent<AIBrain>().TransitionToState("DoNothing");
        
    }
}
