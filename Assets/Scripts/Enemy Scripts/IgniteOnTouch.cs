﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class IgniteOnTouch : MonoBehaviour {

public LayerMask _layerMask;
public bool isFlame = true;

private void Start()
{
        
}

private void OnTriggerEnter2D(Collider2D other)
{
	if(!isActiveAndEnabled)
	{
		return;
	}

	//check to see if what we are colliding with is in the collision LayerMask
	int layer = other.gameObject.layer;
	if (_layerMask != (_layerMask | (1 << layer)))
	{
		return;
	}
	
	//TODO: This will have to be a switch statement to handle different types

	CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
	if (_cloud == null)
	{
		return;
	}

	if (!_cloud.isFlammable || _cloud.isConverted)
	{
		return;
	}

	_cloud.Ignite(isFlame);

}

private void OnTriggerStay2D(Collider2D other) {
	if(!isActiveAndEnabled)
	{
		return;
	}


	//check to see if what we are colliding with is in the collision LayerMask
	int layer = other.gameObject.layer;
	if (_layerMask != (_layerMask | (1 << layer)))
	{
		return;
	}

	CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
	if (_cloud == null)
	{
		return;
	}
		
	if (!_cloud.isFlammable || _cloud.isIgnited || _cloud.isConverted)
	{
		return;
	}

	_cloud.Ignite(isFlame);

}

}
