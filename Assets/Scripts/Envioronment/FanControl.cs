﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class FanControl : MonoBehaviour
{
    //class to attach to fan for control from a switch or pressure plate.
    //Turns the Area Effect on/off
    //Turns the particles on/off
    //Turns the animation on/off

    public ParticleSystem _particles;
    public AudioClip fanLoop;
    public AudioClip fanOff;
    public AudioClip fanOn;

    private AudioSource _audiosource;


    //Called from ButtonAcrivated script on other object
    public void TurnFanOn()
    {
        //1.  Turn off Area Effector
        //2.  Turn off Particle if they exist
        //3.  Change animation state if exists.

        AreaEffector2D _effector = GetComponent<AreaEffector2D>();
        if (_effector != null)
        {
          _effector.enabled = true;
        }
        _audiosource =  SoundManager.Instance.PlaySound(fanLoop,this.transform.position,true);
        _particles.Play();
      
    }

    public void TurnFanOff()
    {
        AreaEffector2D _effector = GetComponent<AreaEffector2D>();
        if (_effector != null)
        {
            _effector.enabled = false;
        }
        _particles.Stop();
        SoundManager.Instance.StopLoopingSound(_audiosource);
        SoundManager.Instance.PlaySound(fanOff,this.transform.position, false);
    }



}
