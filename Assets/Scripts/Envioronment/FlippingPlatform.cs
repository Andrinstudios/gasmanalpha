using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class FlippingPlatform : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public float initialDelay = 0f;
    public float timeBeforeFlip = 2f;

    protected float _initTime;
    protected float _flipTimestamp;
    protected float _delayTimestamp;
    protected bool flip = false;
    protected bool delayed = false;
    protected Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = this.gameObject.GetComponent<Animator>();
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - _delayTimestamp > initialDelay  && delayed)
        {
            delayed = false;
            flip = true;
            _initTime = Time.time;
            Debug.Log("delay ended "+_delayTimestamp);
        }

        if (Time.time - _initTime > timeBeforeFlip  && !delayed)
        {
           // Debug.Log("flip");
            Flip();
        }
    }

    public void Initialize()
    {
        if (initialDelay > 0)
        {
            _delayTimestamp = Time.time;
            delayed = true;
            flip = false;
        }else
        {
            _initTime = Time.time;
            flip = true;
        }
    }

    protected void Flip()
    {
        _initTime = Time.time;
        _animator.SetBool("Flip", flip);
        flip = !flip;
    }

    public virtual void OnMMEvent(CorgiEngineEvent eventType)
    {
        if (eventType.EventType == CorgiEngineEventTypes.Respawn)
        {
            ResetPlatform();
        }
    }

    public virtual void ResetPlatform()
    {
        if (_animator != null)
        {
            _animator.Rebind();
            _animator.Update(0f);
        }

        Initialize();
    }



    protected virtual void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }
}
