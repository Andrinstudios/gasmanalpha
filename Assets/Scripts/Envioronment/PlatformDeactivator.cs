using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDeactivator : MonoBehaviour
{
    public float deactivatedTime = 2f;

    private BoxCollider2D _collider;
    private SpriteRenderer _renderer;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    public void DeactivatePlatform()
    {
        _collider.enabled = false;
        _renderer.enabled = false;
        StartCoroutine(StartTimer());
    }

    protected void ReactivatePlatform()
    {
        _collider.enabled = true;
        _renderer.enabled = true;
    }

    protected IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(deactivatedTime);
        ReactivatePlatform();

    }

}
