using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

/*  extends the AppearDisappear class to allow for animations and stopping the cycle
 *  and returning to the inital state.  Also handles resetting when player dies which the 
 *  base class does not.
 */

public class GMDisappearingPlatform : AppearDisappear, MMEventListener<CorgiEngineEvent>
{
    protected bool changeComing = false;
    protected Vector3 initialPostion = new Vector3();

    //variables for determining if player on top of platform
    protected float _platformTopY;
    protected const float _toleranceY = 0.05f;

    // bool for preventing early reactivation
    protected bool fallTriggered = false;

    protected override void Start()
    {
        //store the initial positon to reset on player respawn
        initialPostion = this.gameObject.transform.position;
        base.Start();
    }


    public override void ChangeState()
    {
        if (_currentState == AppearDisappearStates.HiddenToVisible)
        {
            changeComing = true;
        }

        base.ChangeState();

        if (_currentState == AppearDisappearStates.Visible && changeComing)
        {
            Activate(false);
            changeComing = false;
            fallTriggered = false;
        }
    }

    public override void OnTriggerEnter2D(Collider2D collider)
    {
        CorgiController controller = collider.GetComponent<CorgiController>();
        if (controller == null)
        {
            return;
        }

        // if we're colliding with a character, we check that's it's actually above the platform's top
        _platformTopY = (_collider2D != null) ? _collider2D.bounds.max.y : this.transform.position.y;
        if (controller.ColliderBottomPosition.y < _platformTopY - _toleranceY)
        {
            return;
        }

        if (StartMode != StartModes.PlayerContact)
        {
            return;
        }

        if (!fallTriggered)
        {
            fallTriggered = true;
            _lastStateChangedAt = Time.time;
            Activate(true);
        }
    }

    public virtual void OnMMEvent(CorgiEngineEvent eventType)
    {
        if (eventType.EventType == CorgiEngineEventTypes.Respawn)
        {
            ResetPlatform();
        }
    }

    public virtual void ResetPlatform()
    {
        this.gameObject.transform.position = initialPostion;
        Initialization();
        fallTriggered = false;

        if (_animator != null)
        {
            _animator.Rebind();
            _animator.Update(0f);
        }

    }



    protected virtual void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }
}
