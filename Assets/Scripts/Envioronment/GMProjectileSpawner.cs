using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class GMProjectileSpawner : TimedSpawner
{
	[Tooltip("the amount of time to spawn projectiles, in seconds")]
	public float timeOn = 2f;
	[Tooltip("the amount of time between spawning episodes, in seconds")]
	public float timeOff = 2f;

	
	public Transform spawnPointLeft;
	public Transform spawnPointRight;
	public Transform spawnPointTop;
	public Transform spawnPointBottom;

	protected bool canSpawn = true;
	protected float _spawnTimestamp;


    protected override void Initialization()
    {
        base.Initialization();
		_spawnTimestamp = Time.time;
    }

    protected override void Update()
    {
		if (canSpawn)
        {
			if (Time.time - _spawnTimestamp > timeOn)
            {
				canSpawn = false;
				_spawnTimestamp = Time.time;
            }
        }else
        {
			if (Time.time - _spawnTimestamp > timeOff)
            {
				canSpawn = true;
				_spawnTimestamp = Time.time;
            }
        }


		if (Time.time - _lastSpawnTimestamp > _nextFrequency && canSpawn)
		{
			Spawn();
		}
	}


    protected override void Spawn()
    {
		if (spawnPointLeft != null)
        {
			GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

			// mandatory checks
			if (nextGameObject == null) { return; }
			if (nextGameObject.GetComponent<MMPoolableObject>() == null)
			{
				throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
			}
			// we position the object
			nextGameObject.transform.position = this.transform.position;

			// check for projectile and set direction
			Projectile _projectile = nextGameObject.gameObject.MMGetComponentNoAlloc<Projectile>();
			if (_projectile != null)
			{
				Vector3 _direction = spawnPointLeft.position - this.gameObject.transform.position;
				_projectile.SetDirection(_direction, transform.rotation, true);
			}

			// we activate the object
			nextGameObject.gameObject.SetActive(true);
			nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();

			// we check if our object has an Health component, and if yes, we revive our character
			Health objectHealth = nextGameObject.gameObject.MMGetComponentNoAlloc<Health>();
			if (objectHealth != null)
			{
				objectHealth.Revive();
			}
		}

		if (spawnPointBottom != null)
		{
			GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

			// mandatory checks
			if (nextGameObject == null) { return; }
			if (nextGameObject.GetComponent<MMPoolableObject>() == null)
			{
				throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
			}
			// we position the object
			nextGameObject.transform.position = this.transform.position;

			// check for projectile and set direction
			Projectile _projectile = nextGameObject.gameObject.MMGetComponentNoAlloc<Projectile>();
			if (_projectile != null)
			{
				Vector3 _direction = spawnPointBottom.position - this.gameObject.transform.position;
				_projectile.SetDirection(_direction, transform.rotation, true);
			}

			// we activate the object
			nextGameObject.gameObject.SetActive(true);
			nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();

			// we check if our object has an Health component, and if yes, we revive our character
			Health objectHealth = nextGameObject.gameObject.MMGetComponentNoAlloc<Health>();
			if (objectHealth != null)
			{
				objectHealth.Revive();
			}
		}
		if (spawnPointTop != null)
		{
			GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

			// mandatory checks
			if (nextGameObject == null) { return; }
			if (nextGameObject.GetComponent<MMPoolableObject>() == null)
			{
				throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
			}
			// we position the object
			nextGameObject.transform.position = this.transform.position;

			// check for projectile and set direction
			Projectile _projectile = nextGameObject.gameObject.MMGetComponentNoAlloc<Projectile>();
			if (_projectile != null)
			{
				Vector3 _direction = spawnPointTop.position - this.gameObject.transform.position;
				_projectile.SetDirection(_direction, transform.rotation, true);
			}

			// we activate the object
			nextGameObject.gameObject.SetActive(true);
			nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();

			// we check if our object has an Health component, and if yes, we revive our character
			Health objectHealth = nextGameObject.gameObject.MMGetComponentNoAlloc<Health>();
			if (objectHealth != null)
			{
				objectHealth.Revive();
			}
		}
		if (spawnPointRight != null)
		{
			GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

			// mandatory checks
			if (nextGameObject == null) { return; }
			if (nextGameObject.GetComponent<MMPoolableObject>() == null)
			{
				throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
			}
			// we position the object
			nextGameObject.transform.position = this.transform.position;

			// check for projectile and set direction
			Projectile _projectile = nextGameObject.gameObject.MMGetComponentNoAlloc<Projectile>();
			if (_projectile != null)
			{
				Vector3 _direction = spawnPointRight.position - this.gameObject.transform.position;
				_projectile.SetDirection(_direction, transform.rotation, true);
			}

			// we activate the object
			nextGameObject.gameObject.SetActive(true);
			nextGameObject.gameObject.MMGetComponentNoAlloc<MMPoolableObject>().TriggerOnSpawnComplete();

			// we check if our object has an Health component, and if yes, we revive our character
			Health objectHealth = nextGameObject.gameObject.MMGetComponentNoAlloc<Health>();
			if (objectHealth != null)
			{
				objectHealth.Revive();
			}
		}



		// we reset our timer and determine the next frequency
		_lastSpawnTimestamp = Time.time;
		DetermineNextFrequency();
	}
}
