using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MoreMountains.CorgiEngine;

public class ExitUpdater : MonoBehaviour
{
    public UnityEvent characterDeath;
    protected Health _health;

    private void Start()
    {
        _health = GetComponent<Health>();
    }

    protected virtual void OnDeath()
    {
        Debug.Log("cahr died...Invoking");
        characterDeath.Invoke();
    }

    private void OnEnable()
    {
        if (_health == null)
        {
            _health = GetComponent<Health>();
        }

        if (_health != null)
        {
            _health.OnDeath += OnDeath;
        }
    }

    private void OnDisable()
    {
        _health.OnDeath -= OnDeath;
    }
}
