using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


/* what needs to be accomplished:
 * - Brief warning covering the area
 * - run the animation
 * - activate damageOnTouch
 * - apply force to player
 * - disappear
 */


public class GroundAttack : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public MMFeedbacks warningFeedbacks;
    public MMFeedbacks activateFeedback;

    public float warningTime = 1f;
    public float damageTime = 2f;

    protected DamageOnTouch _damageOnTouch;
    protected SpriteRenderer _renderer;
    protected IEnumerator attackSequence;

    protected void Start()
    {
        Initialize();
    }

    protected void Initialize()
    {
        _damageOnTouch = GetComponent<DamageOnTouch>();
        _damageOnTouch.enabled = false;
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.enabled = false;

        attackSequence = BeginSequence();
        Activate();
        
    }

    public virtual void Activate()
    {
        StartCoroutine(attackSequence);
    }

    protected virtual IEnumerator BeginSequence()
    {
        // Need to wait a frame to allow particles/feedback to get set
        yield return new WaitForEndOfFrame();

        ShowWarning();
        yield return new WaitForSeconds(warningTime);

        StartAttack();
        yield return new WaitForSeconds(damageTime);

        EndAttack();
    }

    protected virtual void ShowWarning()
    {
        warningFeedbacks?.PlayFeedbacks();
    }

    protected virtual void StartAttack()
    {
        _renderer.enabled = true;
        activateFeedback?.PlayFeedbacks();
        _damageOnTouch.enabled = true;
    }

    protected virtual void EndAttack()
    {
        _damageOnTouch.enabled = false;
        Destroy(this.gameObject);
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                Reset();

                break;
            default:
                break;
        }
    }

    protected virtual void Reset()
    {
        gameObject.SetActive(false);
        StopAllCoroutines();
        EndAttack();
        
    }
}
