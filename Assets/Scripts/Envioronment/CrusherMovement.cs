using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;

public class CrusherMovement : MovingPlatform
{
	public float initialDelay = 0f;
    public AnimationCurve ReboundAcceleration = new AnimationCurve(new Keyframe(0, 1f), new Keyframe(1f, 0f));

	bool isRebounding = false;
	bool isDelayed = true;



    protected override void Start()
    {
        base.Start();
		StartCoroutine(StartDealy());
    }

    protected override void Update()
    {
		if(isDelayed)
        {
			return;
        }

        base.Update();
    }

    IEnumerator StartDealy()
    {
		Debug.Log("delaying");
		yield return new WaitForSeconds(initialDelay);
		isDelayed = false;
    }

    protected override void PointReached()
    {
		//check to see if this is the bottom of the crushing cycle
		if (isRebounding)
		{
			Debug.Log("end reached");
			EndReachedFeedback?.PlayFeedbacks(this.transform.position);
        }
        else
        {
			PointReachedFeedback?.PlayFeedbacks(this.transform.position);
        }

		isRebounding = !isRebounding;
	}

    public override void MoveAlongThePath()
    {
		switch (AccelerationType)
		{
			case PossibleAccelerationType.ConstantSpeed:
				transform.position = Vector3.MoveTowards(transform.position, _originalTransformPosition + _currentPoint.Current, Time.deltaTime * MovementSpeed);
				break;

			case PossibleAccelerationType.EaseOut:
				transform.position = Vector3.Lerp(transform.position, _originalTransformPosition + _currentPoint.Current, Time.deltaTime * MovementSpeed);
				break;

			case PossibleAccelerationType.AnimationCurve:

				if (isRebounding)
                {
					//Debug.Log("crushing");
					float distanceBetweenPoints = Vector3.Distance(_previousPoint, _currentPoint.Current);

					if (distanceBetweenPoints <= 0)
					{
						return;
					}

					float remappedDistance = 1 - MMMaths.Remap(_distanceToNextPoint, 0f, distanceBetweenPoints, 0f, 1f);
					float speedFactor = Acceleration.Evaluate(remappedDistance);

					transform.position = Vector3.MoveTowards(transform.position, _originalTransformPosition + _currentPoint.Current, Time.deltaTime * MovementSpeed * speedFactor);
					break;
                }
                else
                {
					//Debug.Log("rebounding");
					float distanceBetweenPoints = Vector3.Distance(_previousPoint, _currentPoint.Current);

					if (distanceBetweenPoints <= 0)
					{
						return;
					}

					float remappedDistance = 1 - MMMaths.Remap(_distanceToNextPoint, 0f, distanceBetweenPoints, 0f, 1f);
					float speedFactor = ReboundAcceleration.Evaluate(remappedDistance);

					transform.position = Vector3.MoveTowards(transform.position, _originalTransformPosition + _currentPoint.Current, Time.deltaTime * MovementSpeed * speedFactor);
					break;
				}


		}
	}
}
