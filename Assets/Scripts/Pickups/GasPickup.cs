﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public struct GasIncreaseEvent
	{
		public PickableItem PickedItem;
		/// <summary>
		/// Initializes a new instance of the <see cref="MoreMountains.CorgiEngine.GasIncreaseEvent"/> struct.
		/// </summary>
		/// <param name="pickedItem">Picked item.</param>
		public GasIncreaseEvent(PickableItem pickedItem)
		{
			PickedItem = pickedItem;
		}
        
        static GasIncreaseEvent e;
        public static void Trigger(PickableItem pickedItem)
        {
            e.PickedItem = pickedItem;
            MMEventManager.TriggerEvent(e);
        }
    }



    public class GasPickup : GMPickableItem
    {
        public float gasAmountToAdd = 10f;

        public override void OnTriggerEnter2D(Collider2D collider)
        {
            //if the tank is full do nothing and return
            if (GasManager.Instance.isFull)
            {
                return;
            }

                base.OnTriggerEnter2D(collider);
        }

        public override void PickItem(GameObject picker)
        {
            GasManager.Instance.AddGas(gasAmountToAdd);
            MMEventManager.TriggerEvent(new GasIncreaseEvent(this));
        }

    }

    
}
