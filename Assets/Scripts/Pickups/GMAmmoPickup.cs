﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public enum AmmoPickupEventTypes
{
    addAmmo,
    useAmmo
}

public struct AmmoPickUpEvent
{
    public AmmoPickupEventTypes EventType;
    public GMAmmoPickup AmmoPickup;


    public AmmoPickUpEvent(GMAmmoPickup ammoPickup, AmmoPickupEventTypes eventType)
    {
        AmmoPickup = ammoPickup;
        EventType = eventType;
    }

    static AmmoPickUpEvent e;
    public static void Trigger(GMAmmoPickup ammoPickup, AmmoPickupEventTypes eventType)
    {
        e.AmmoPickup = ammoPickup;
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}

public class GMAmmoPickup : PickableItem, MMEventListener<GMAmmoPickupToggleEvent>
{
	protected bool isPickable = true;


    private void OnEnable()
    {
		this.MMEventStartListening<GMAmmoPickupToggleEvent>();
    }

    private void OnDisable()
    {
		this.MMEventStopListening<GMAmmoPickupToggleEvent>();
    }

	public void OnMMEvent(GMAmmoPickupToggleEvent pickupEvent)
    {

        if (pickupEvent.EventType == GMAmmoPickupToggleEventTypes.on)
        {
            isPickable = true;
        }

        if (pickupEvent.EventType == GMAmmoPickupToggleEventTypes.off)
        {
            isPickable = false;
        }
			
		
    }

    public override void PickItem(GameObject picker)
    {
        if (CheckIfPickable())
        {
            Effects();
            AmmoPickUpEvent.Trigger(this, AmmoPickupEventTypes.addAmmo);
            PickItem(picker);
            if (DisableObjectOnPick)
            {
                // we desactivate the gameobject
                gameObject.SetActive(false);
            }
            else
            {
                if (DisableColliderOnPick)
                {
                    _collider.enabled = false;
                }
                if (DisableRendererOnPick && (_renderer != null))
                {
                    _renderer.enabled = false;
                }
            }
        }

    }


    protected override bool CheckIfPickable()
    {
        // if what's colliding with the coin ain't a characterBehavior, we do nothing and exit
        _character = _pickingCollider.GetComponent<Character>();
        if (_character == null)
        {
            return false;
        }
        if (_character.CharacterType != Character.CharacterTypes.Player)
        {
            return false;
        }
        if (_itemPicker != null)
        {
            if (!_itemPicker.Pickable())
            {
                return false;
            }
        }

        if (!isPickable) { return false; }

        return true;
    }
}
