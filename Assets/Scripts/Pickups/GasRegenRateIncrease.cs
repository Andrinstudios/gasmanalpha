﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GasRegenRateIncrease : GMPickableItem
{
    public float RateIncreasePercent = 10;

    public override void PickItem(GameObject picker)
    {
        GasManager.Instance.ChangeGasRegenRate(RateIncreasePercent);
    }

}
