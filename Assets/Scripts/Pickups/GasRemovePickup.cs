﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{



    public class GasRemovePickup : GMPickableItem
    {

        public float gasToRemove = 30f;

        public override void PickItem(GameObject picker)
        {
            GasManager.Instance.RemoveGas(gasToRemove);
        }
    }
}
