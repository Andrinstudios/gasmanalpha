﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GasCapacityIncrease : GMPickableItem
{
    public float maxLevelIncrease = 10;

    public override void PickItem(GameObject picker)

    {
        GasManager.Instance.ChangeMaxGasLevel(maxLevelIncrease);  
    }

}
