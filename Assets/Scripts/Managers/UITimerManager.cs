﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


// handles the starting of and callbacks for the UI timers in the game.
public class UITimerManager : MMPersistentSingleton<UITimerManager>
{
    public Image coolDownTimerImage;
    public MMFeedbacks abilityReadyFeedback;
    
    private float coolDownTime;
    private float countDownTimer;

    bool isRunning= false;

    private void Update() {
        if (isRunning)
        {
            AdvanceTimer();
        }
    }
    // starts the cool down timer and then hands it off to the update/advance timer loop.
    public void StartCooldownTimer(float cdTime)
    {
        coolDownTime = cdTime;
        countDownTimer = cdTime;
        isRunning = true;
    }
    // handles the actual count down and drawin of the timer
    private void AdvanceTimer()
    {
        if (countDownTimer >= 0)
		{
            if (coolDownTimerImage == null)
            {
                Debug.Log("**Please set the timer image in the UITimerManager");
                return;
            }
			coolDownTimerImage.fillAmount = countDownTimer/coolDownTime;
			countDownTimer -= Time.deltaTime;			
		}else{ isRunning = false; }
    }
    //called from outside object to start the timer and handle the callback when it is done.
    public IEnumerator ReturnTest(float cdTime, System.Action<bool> callback)
    {
        StartCooldownTimer(cdTime);
        yield return new WaitForSeconds(cdTime);
        callback (false);
    }

}
