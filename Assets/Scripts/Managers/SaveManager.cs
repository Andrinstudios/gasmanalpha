﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using Sirenix.OdinInspector;


public enum GMEventTypes
{
    SceneTriggerExit,
    SceneTriggerEnter,
    Save,
    Load
}

public struct GMEvent
{
    public string SceneName;
    public GMEventTypes EventType;
    public GMEvent(GMEventTypes eventType, string sceneName)
    {
        EventType = eventType;
        SceneName = sceneName;
    }

    static GMEvent e;
    public static void Trigger(GMEventTypes eventType)
    {
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}

public enum GMDataEventTypes
{
    Enemy,
    Item
}

public struct GMDataEvent
{
    public string SceneName;
    public string ID;
    public EnemyData EData;
    public GMDataEventTypes EventType;

    public GMDataEvent(GMDataEventTypes eventType, string sceneName, string id, EnemyData enemyData)
    {
        EventType = eventType;
        SceneName = sceneName;
        ID = id;
        EData = enemyData;
    }

    static GMDataEvent e;
    public static void Trigger(GMDataEventTypes eventType)
    {
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }
}


public class SaveManager : MMPersistentSingleton<SaveManager>, MMEventListener<CorgiEngineEvent>, MMEventListener<GMEvent>
{
    //create different SaveSettings for various tasks (Init and scene change)
    public ES3Settings initSettings;
    public ES3Settings sceneSettings;

    [ShowInInspector]
    public Dictionary<string, string> RespawnControlDict = new Dictionary<string, string>();

    public string initFileName = "initSave.es3";
    public string sceneFileName = "sceneSave.es3";


    protected void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
        this.MMEventStartListening<GMEvent>();

        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    protected override void Awake()
    {
        //intial settings (restart, death)
        initSettings = new ES3Settings(ES3.EncryptionType.None);
        initSettings.path = initFileName;


        //scene load unload settings
        sceneSettings = new ES3Settings(ES3.EncryptionType.None);
        sceneSettings.path = sceneFileName;

        InitializeRespawnDictionary();

    }


    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
        this.MMEventStopListening<GMEvent>();

        SceneManager.sceneLoaded -= OnSceneLoaded;

        ES3.DeleteFile(initSettings);
        ES3.DeleteFile(sceneSettings);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("loaded scene " + scene.name);
        SetSceneToReload(scene.name);
    }

    //catch the sceneEnter/Exit events and act on them
    public void OnMMEvent(GMEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case GMEventTypes.SceneTriggerExit:
                SetSceneToReload(thisEvent.SceneName);
                break;

            default:
                break;
        }

    }


    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        //if the player dies we want ot reset the level so delete the file to default to initial settings.

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:

                Debug.Log("player died setting Respawn to true");
                //set a flag to let everyone know we died
                SetRespawnDictToReset();
                SceneTest();
                break;

            case CorgiEngineEventTypes.LevelStart:
                Debug.Log("LevelStart setting up dict");
              //  InitializeRespawnDictionary();
                break;

            default:
                break;
        }
    }

    private void ResetScenes()
    {
        //clear the tracking file to reset the game at the current checkpoint
        foreach (var key in ES3.GetKeys(initFileName))
        {
            ES3.Save<bool>(key, false, initSettings);
        }

        foreach (var key in ES3.GetKeys(sceneFileName))
        {
            ES3.DeleteKey(key, sceneSettings);
        }
    }

    private void InitializeRespawnDictionary()
    {
        if (SceneManager.sceneCount > 0)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                RespawnControlDict.Add(scene.name, "reset");
            }
        }

    }

    private void SetRespawnDictToReset()
    {
        List<string> keyList = new List<string>(this.RespawnControlDict.Keys);
        foreach (string key in keyList)
        {
            Debug.Log("reset " + key);
            RespawnControlDict[key] = "reset";
        }
    }

    public void SetSceneToReload(string sceneName)
    {
        Debug.Log("set to reload: "+sceneName);
        RespawnControlDict[sceneName] = "reload";
    }

    public bool ShouldReset(string sceneName)
    {
        string actionToTake = RespawnControlDict[sceneName];
        bool shouldReset = false;

        switch (actionToTake)
        {
            case "reset":
                shouldReset = true;
                break;

            case "reload":
                shouldReset = false;
                break;

            default:
                break;

        }
        return shouldReset;
    }

    private void SceneTest()
    {
        if (SceneManager.sceneCount > 0)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                Debug.Log("scene test " + scene.name);
            }
        }

    }

}



