using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//  This class is for creating variation in animation speed when there are
//  multiple objects of the same type in the sceene
//  Add a float parameter to the animator called "SpeedAdjust" then add
//  this script to the object which has the animator on it.

public class AnimationSpeedVariation : MonoBehaviour
{
    protected Animator _animator;
    public float animationSpeedFactor = 1.2f;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.SetFloat("SpeedAdjust", animationSpeedFactor);

        
    }
}
