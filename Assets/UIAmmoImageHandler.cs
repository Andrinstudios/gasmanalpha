﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class UIAmmoImageHandler : MonoBehaviour, MMEventListener<AmmoPickUpEvent>, MMEventListener<CorgiEngineEvent>
{
    public MMProgressBar progressBar;
    protected float currentValue;
    private float minValue = 0f;

    //this is number of pickups needed for the gun to become active
    public float maxValue = 5f;

    public MMFeedbacks readyFeedbacks;
    public MMFeedbacks emptyFeedbacks;



    private void OnEnable()
    {
        this.MMEventStartListening<AmmoPickUpEvent>();
        this.MMEventStartListening<CorgiEngineEvent>();

    }

    private void OnDisable()
    {
        this.MMEventStopListening<AmmoPickUpEvent>();
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public void OnMMEvent(AmmoPickUpEvent ammoEvent)
    {
        if (ammoEvent.EventType == AmmoPickupEventTypes.addAmmo)
        {
            currentValue ++;
        }

        if (ammoEvent.EventType == AmmoPickupEventTypes.useAmmo)
        {
            currentValue --;
        }

       // Debug.Log("bar value " + currentValue);
        UpdateBar();
        FeedbackCheck();
      
    }

    public void OnMMEvent(CorgiEngineEvent ceEvent)
    {
        if (ceEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            currentValue = 0;
            UpdateBar();
        }
    }


    protected void UpdateBar()
    {
        progressBar.UpdateBar(currentValue, minValue, maxValue);
    }

    protected void FeedbackCheck()
    {
        if (currentValue == maxValue)
        {
            readyFeedbacks?.PlayFeedbacks();
        }

        if (currentValue == 0)
        {
            emptyFeedbacks?.PlayFeedbacks();
        }
    }

}
